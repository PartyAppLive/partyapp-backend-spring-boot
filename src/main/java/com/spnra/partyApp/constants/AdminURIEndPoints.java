package com.spnra.partyApp.constants;

public class AdminURIEndPoints {
    public static final String PARTY_APP_ADMIN_BASE_URL = "party_app/admin";
    public static final String CLUB_CREATE_USER = "/club";
    public static final String CLUB_SIGN_IN = "/club/sign_in";
    public static final String CLUB_SIGN_OUT = "/club/sign_out";
    public static final String CLUB_RESEND_OTP = "/club/resend_otp";
    public static final String CLUB_VERIFY_OTP = "/club/verify_otp";
    public static final String CLUB_GET_USER = "/get_club";
    public static final String CLUB_PROFILE_UPDATE = "/club/profile_update";

    public static final String UPDATE_RESERVATION = "/update_reservation";

}
