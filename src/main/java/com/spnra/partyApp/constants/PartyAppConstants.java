package com.spnra.partyApp.constants;

import java.util.Arrays;
import java.util.List;

public class PartyAppConstants {

    public static final String accessToken = "cGFydHlBcHAtTy1TZXJ2aWNl";
    public static final String S3_DOC_URL = "https://s3.ap-south-1.amazonaws.com/party.app/Profile-pictures/";
    public static final String S3_EXTRA_DOC_URL = "https://s3.ap-south-1.amazonaws.com/party.app/club-pictures/";
    public static final String beforeMsg = "&text=Your%20PARTYAPP%20verification%20(OTP)%20code%20is%20";
    public static final String afterMsg = ".%20Code%20can%20be%20used%20one%20time%20only.%20Please%20DO%20NOT%20SHARE%20this%20OTP%20with%20anyone.%20Cheers!%20PARTYAPP%20-%20SPNRAT&priority=ndnd&stype=normal";
    public static final String firstMsg = "user=SPNRA&pass=123456&sender=SPNRAT&phone=";
    public static final String contactUsMsg = "Feel free to contact us at xyz@gmail or call us at 8145763666 or 7865809148";
    public static final List<String> CANCELLED_RESERVATION_STATUS = Arrays.asList((new String[]{"no_show"}));
    public static final List<String> ALLOWED_FILE_TYPE = Arrays.asList((new String[]{"application/pdf","image/png","image/jpeg","jpg"}));

}
