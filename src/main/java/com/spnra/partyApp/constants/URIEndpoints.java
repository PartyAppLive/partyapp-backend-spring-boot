package com.spnra.partyApp.constants;

public class URIEndpoints {

    public static final String PARTY_APP_BASE_URL = "party_app";
    public static final String PARTY_APP_ADMIN_BASE_URL = "party_app/admin";
    public static final String CREATE_USER = "/user";
    public static final String SIGN_IN = "/user/sign_in";
    public static final String SIGN_OUT = "/user/sign_out";
    public static final String RESEND_OTP = "/user/resend_otp";
    public static final String VERIFY_OTP = "/user/verify_otp";
    public static final String RESERVATION = "/reservation";
    public static final String GET_RESERVATIONS = "/reservations";
    public static final String CANCEL_RESERVATION = "/cancel_reservation";
    public static final String CREATE_EVENT = "/event";
    public static final String CREATE_CLUB = "/club";
    public static final String GET_CLUBS = "/clubs";
    public static final String GET_OFFER_CLUBS = "/offer_clubs";
    public static final String GET_USER = "/get_user";
    public static final String GET_LOCATIONS = "/locations";
    public static final String SELECT_LOCATION = "/location";
    public static final String CONTACT_US = "/contact";
    public static final String GET_CLUB = "/club/{club_id}";
    public static final String CREATE_CLUB_DETAIL = "/club_detail";
    public static final String EVENT_INTEREST = "/event/interest";
    public static final String EVENT_PEOPLE_INTEREST = "/event/people_interest";
    public static final String REMOVE_EVENT_INTEREST = "/event/remove_interest";
    public static final String EVENTS = "/events";
    public static final String EVENTS_TODAY = "/events/today";
    public static final String SEARCH_EVENTS = "/events/search";
    public static final String IMAGE_UPLOAD = "/user/image_upload";
    public static final String PROFILE_UPDATE = "/user/profile_update";
    public static final String CLUB_IMAGES_UPLOAD = "/club/image_upload";
    public static final String CLUB_IMAGE_INFO_UPDATE = "/club/image_info_update";
    public static final String CLUB_INTEREST = "/club/interest";
    public static final String FAVOURITE_CLUBS = "/club/interests";
    public static final String SEARCH_CLUBS = "/club/search";
    public static final String REMOVE_CLUB_INTEREST = "/club/remove_interest";
    public static final String CREATE_OFFER = "/offer";
    public static final String GET_OFFERS = "/offers/{club_id}";
    public static final String CREATE_TICKET = "/ticket";
    public static final String GET_TICKET = "/ticket/{event_id}";
    public static final String GET_CART = "/cart/get-products";
    public static final String ADD_PRODUCT = "/cart/add-product";
    public static final String REMOVE_PRODUCT = "/cart/remove-product";
    public static final String CART_VALIDATE = "/cart/cart-validate";
    public static final String PLACE_ORDER = "/cart/order";
    public static final String PLACE_TICKET_ORDER = "/ticket/order";
}
