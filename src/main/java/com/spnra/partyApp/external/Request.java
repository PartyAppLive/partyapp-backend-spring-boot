package com.spnra.partyApp.external;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
public class Request<R, B> {

    private B body;

    private HttpHeaders headers;

    private String url;

    private HttpMethod method;

    private Class<R> responseType;
}
