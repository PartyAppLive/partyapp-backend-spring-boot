package com.spnra.partyApp.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "offers")
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "club_id")
    private Long clubId;

    @Column(name = "image")
    private String image;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "timing")
    private String timing;

    @Column(name = "`desc`")
    private String desc;

    @Column(name = "original_price")
    private Integer originalPrice;

    @Column(name = "discounted_price")
    private Integer discountedPrice;

    @Column(name = "discount")
    private Integer discount;

    @Column(name = "offer_type")
    private String offerType;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "sold_unit")
    private Integer soldUnit;

    @Column(name = "inventory")
    private Integer inventory;

    @Column(name = "terms")
    private String terms;

    @Column(name="best_seller")
    private Boolean bestSeller;

    @Column(name = "drinks_inclusions")
    private String drinksInclusions;

    @Column(name = "food_inclusions")
    private String foodInclusions;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;
}
