package com.spnra.partyApp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "detail")
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Detail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "club_id")
    private Long clubId;

    @Column(name = "description")
    private String description;

    @Column(name = "beer_text")
    private String beerText;

    @Column(name = "kitchen_text")
    private String kitchenText;

    @Column(name = "beer_rate")
    private Integer beerRate;

    @Column(name = "kitchen_rate")
    private Integer kitchenRate;

    //json
    @Column(name = "photos")
    private String photos;

    //json
    @Column(name = "day_special")
    private String daySpecial;

    @Column(name = "stags")
    private String stags;

    @Column(name = "timings")
    private String timings;

    //json
    @Column(name = "menu")
    private String menu;

    @Column(name = "valet")
    private Boolean valet;

    @Column(name = "reservation")
    private Boolean reservation;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;
}
