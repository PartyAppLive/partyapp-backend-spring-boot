package com.spnra.partyApp.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "events")
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "event_name")
    private String eventName;

    @Column(name = "club_id")
    private Long clubId;

    @Column(name = "ticket_id")
    private Long ticketId;

    @Column(name = "ticket_info")
    private String ticketInfo;

    @Column(name = "image")
    private String image;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "organiser")
    private String organiser;

    @Column(name = "venue")
    private String venue;

    @Column(name = "address")
    private String address;

    @Column(name = "organiser_number")
    private String organiserNumber;

    @Column(name = "interest")
    private Integer interest;

    @Column(name = "description")
    private String description;

    @Column(name = "event_date")
    private Date eventDate;

    @Column(name="cityId")
    private Long city_id;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "event_time")
    private String eventTime;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;

}
