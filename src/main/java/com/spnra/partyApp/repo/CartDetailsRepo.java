package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.CartDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartDetailsRepo extends JpaRepository<CartDetails,Long> {

    Optional<List<CartDetails>> findByCartId(Long userId);


    @Transactional
    void deleteAllByIdIn(List<Long> id);
}
