package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Interest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface InterestRepo  extends JpaRepository<Interest,Long> {

    Optional<Interest> findAllByEventIdAndUserId(Long eventId, Long userId);

    @Transactional
    void deleteById(Long id);

    @Query(nativeQuery = true,value = "select * from `interest` where event_id = ?1 limit ?2,?3")
    List<Map<String,Object>> fetchAllInterestedPeopleBasedOnEventId(Integer eventId, long offset, int noOfEntry);

    Integer countAllByEventId(Long eventId);

    List<Interest> findByUserIdAndEventIdIn(Long userId, HashSet<Long> eventId);

}
