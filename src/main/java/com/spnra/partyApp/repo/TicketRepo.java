package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepo extends JpaRepository<Ticket,Long> {

    Optional<List<Ticket>> findByEventId(Long eventId);
}
