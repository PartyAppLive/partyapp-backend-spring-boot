package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.ClubInterest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ClubInterestRepo extends JpaRepository<ClubInterest,Long> {

    Optional<ClubInterest> findAllByClubIdAndUserId(Long clubId, Long userId);

    @Query(nativeQuery = true,value = "select * from favourite where user_id = ?1 and city_id = ?2 limit ?3,?4")
    List<Map<String,Object>> fetchAllFavouriteClubsBasedOnUserAndStatus(Long userId,Long cityId, long offset, int noOfEntry);

    @Query(nativeQuery = true,value = "select count(*) from favourite where user_id = ?1 and city_id = ?2")
    Long getAllFavouriteClubsCountByCityAndStatus(Long userId,Long cityId);

    @Transactional
    void deleteById(Long id);
}
