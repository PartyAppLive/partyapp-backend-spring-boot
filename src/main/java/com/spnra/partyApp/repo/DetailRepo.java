package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Detail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DetailRepo extends JpaRepository<Detail,Long> {

    Optional<Detail> findByClubId(Long id);
}
