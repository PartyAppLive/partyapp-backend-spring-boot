package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface EventRepo extends JpaRepository<Event,Long> {

    @Query(nativeQuery = true,value = "select * from events where event_date >=CURRENT_DATE and city_id = ?1 and status = 1 order by event_date asc limit ?2,?3")
    List<Map<String,Object>> fetchAllEventsBasedOnCity(Long cityId,long offset, int noOfEntry);

    @Query(nativeQuery = true,value = "select count(*) from events where event_date >=CURRENT_DATE and city_id = ?1 and status = 1")
    Long getAllEventsCountBasedOnCity(Long cityId);

    @Query(nativeQuery = true,value = "select * from events where event_date = CURRENT_DATE and city_id = ?1 and status = 1 limit ?2,?3")
    List<Map<String,Object>> fetchAllTodayEventsBasedOnCity(Long cityId,long offset, int noOfEntry);

    @Query(nativeQuery = true,value = "select count(*) from events where date(event_date) = CURRENT_DATE and city_id = ?1 and status = 1")
    Long getAllTodayEventsCountBasedOnCity(Long cityId);

    @Query(nativeQuery = true,value = "select * from events where event_name like %?1%")
    List<Map<String,Object>> searchEventBasedOnKeyword(String keyWord);

}
