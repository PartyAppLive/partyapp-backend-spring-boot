package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepo extends JpaRepository<Cart,Long> {

    Optional<Cart> findByUserIdAndStatus(Long userId,Boolean status);
}
