package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface ReservationRepo extends JpaRepository<Reservation,Long> {


    @Query(nativeQuery = true,value = "select * from reservation where user_id = ?1 and booked_date>?2 and status not  in (?3) order by booked_date asc limit ?4,?5")
    List<Map<String,Object>> findByUserIdAndBookedDateAfter(Long city,Date date,List<String> status, long offset, int noOfEntry);

    @Query(nativeQuery = true,value = "select count(*) from reservation where user_id = ?1 and booked_date>?2 and status not in (?3)")
    Long countReservationByBookedDateAfter(Long userId,Date date,List<String> status);


}
