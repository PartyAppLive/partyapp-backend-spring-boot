package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepo extends JpaRepository<Location,Long> {

    List<Location> findAll();
}
