package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepo extends JpaRepository<Token,Long> {

    Optional<Token> findByUserIdAndUserTypeAndTypeAndStatus (Long userId,Integer userType,String type,Boolean status);

    Optional<Token> findByUserIdAndUserTypeAndType(Long userId,Integer userType,String type);

}
