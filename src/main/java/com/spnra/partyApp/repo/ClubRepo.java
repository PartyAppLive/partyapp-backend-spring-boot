package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Club;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface ClubRepo extends JpaRepository<Club,Long> {

    @Query(nativeQuery = true,value = "select * from club where city_id = ?1 and status = 1 order by priority desc limit ?2,?3")
    List<Map<String,Object>> fetchAllClubsBasedOnCityAndStatus(Long city, long offset, int noOfEntry);

    @Query(nativeQuery = true,value = "select count(*) from club where city_id = ?1 and status = 1")
    Long getAllClubCountByCityAndStatus(Long city);

    @Query(nativeQuery = true,value = "select * from club where city_id = ?1 and status = 1 and discount is not null order by priority desc limit ?2,?3")
    List<Map<String,Object>> fetchAllClubsBasedOnCityAndStatusAndDiscount(Long city, long offset, int noOfEntry);

    @Query(nativeQuery = true,value = "select * from club where city_id = ?1 and name like %?2%")
    List<Map<String,Object>> searchClubBasedOnKeyword(Long city, String keyWord);

    @Query(nativeQuery = true,value = "select count(*) from club where city_id = ?1 and status = 1 and discount is not null")
    Long getAllClubCountByCityAndStatusAndDiscount(Long city);

    Optional<Club> findByIdAndStatus(Long clubId,Boolean status) ;

    Optional<Club> findById(Long clubId) ;

    List<Club> findByIdInAndStatus(HashSet<Long> id, Boolean status);
}
