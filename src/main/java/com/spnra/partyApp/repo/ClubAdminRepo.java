package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.ClubAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClubAdminRepo extends JpaRepository<ClubAdmin,Long> {

    Optional<ClubAdmin> findByPhoneNumber(String phone);
}
