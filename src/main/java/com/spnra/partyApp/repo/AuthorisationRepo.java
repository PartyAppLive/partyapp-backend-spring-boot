package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Authorisation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface AuthorisationRepo extends JpaRepository<Authorisation,Long> {

    Optional<Authorisation> findByUuidAndUserType(String uuid,Integer type);

    @Transactional
    Long deleteAllByUserId(Long userId);



}
