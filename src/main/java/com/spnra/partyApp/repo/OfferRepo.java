package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Repository
public interface OfferRepo extends JpaRepository<Offer,Long> {

    //when inventory and units sold gets equal we make the status as 0.
    List<Offer> findAllByClubIdAndStatusAndEndDateAfter(Long clubId, Boolean status, Date date);

    List<Offer> findAllByIdIn(HashSet<Long> offerIdList);

}


