package com.spnra.partyApp.repo;

import com.spnra.partyApp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,Long> {

    Optional<User> findByPhoneNumber(String phone);

    List<User> findByIdInAndStatus(HashSet<Long> id, Boolean status);

    Optional<User> findById(Long id);

    Optional<User> findByShareToken(String token);

}
