package com.spnra.partyApp.auth;

import com.spnra.partyApp.constants.PartyAppConstants;
import com.spnra.partyApp.exceptions.AccessTokenMissingException;
import com.spnra.partyApp.exceptions.AuthenticationFailedException;
import com.spnra.partyApp.utils.RequestUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Aspect
@Component
public class ClientAuthInterceptor {

    @Autowired
    private RequestUtil requestUtil;

    @Before("@within(com.spnra.partyApp.auth.ClientAuthentication) "
            + "|| @annotation(com.spnra.partyApp.auth.ClientAuthentication)")
            public void validate(JoinPoint joinPoint) throws AuthenticationFailedException, AccessTokenMissingException {
        String token = requestUtil.getHeader("access_token");
        if(ObjectUtils.isEmpty(token)){
            throw new AccessTokenMissingException("Access Token is missing");
        }
        if(!token.equalsIgnoreCase(PartyAppConstants.accessToken)){
            throw new AuthenticationFailedException("Authentication Failed");
        }
    }
}
