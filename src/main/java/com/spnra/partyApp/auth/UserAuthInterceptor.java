package com.spnra.partyApp.auth;


import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.enums.UserType;
import com.spnra.partyApp.exceptions.AuthenticationFailedException;
import com.spnra.partyApp.exceptions.AuthorizationFailedException;
import com.spnra.partyApp.exceptions.AuthorizationMissingException;
import com.spnra.partyApp.exceptions.SessionExpiredException;
import com.spnra.partyApp.repo.AuthorisationRepo;
import com.spnra.partyApp.utils.RequestUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.Optional;

@Aspect
@Component
public class UserAuthInterceptor {

    @Autowired
    private RequestUtil requestUtil;

    @Autowired
    private AuthorisationRepo authorisationRepo;

    private static final InheritableThreadLocal<Authorisation> userStore = new InheritableThreadLocal<>();

    @Before("@within(com.spnra.partyApp.auth.UserAuthentication) "
            + "|| @annotation(com.spnra.partyApp.auth.UserAuthentication)")
    public Authorisation validate(JoinPoint joinPoint) throws Exception {
        String token = requestUtil.getHeader("authorization");
        if(ObjectUtils.isEmpty(token)){
            throw new AuthorizationMissingException("Authorization Token is missing");
        }
        Optional<Authorisation> authorisation = authorisationRepo.findByUuidAndUserType(token, UserType.NORMAL.getValue());
        if(ObjectUtils.isEmpty(authorisation)){
            throw new AuthorizationFailedException("Wrong authorization token");
        }
        if(authorisation.get().getExpiryTime().before(new Date(System.currentTimeMillis()))){
            throw new SessionExpiredException("Session Expired. Please re-login");
        }
        setThreadLocals(authorisation.get());
        return authorisation.get();
    }

    @After("@within(com.spnra.partyApp.auth.UserAuthentication) "
            + "|| @annotation(com.spnra.partyApp.auth.UserAuthentication)")
    public void clearThreadLocals() {
        userStore.remove();
    }

    public Authorisation getAuthorisationDetails() {
        return userStore.get();
    }

    public void setAuthorisationDetails(Authorisation authorisation) {
        userStore.set(authorisation);
    }

    public void setThreadLocals(Authorisation authorisation) {
        setAuthorisationDetails(authorisation);
    }
}
