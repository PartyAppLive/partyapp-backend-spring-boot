package com.spnra.partyApp.service;

import com.spnra.partyApp.exceptions.CommonException;
import com.spnra.partyApp.exceptions.RateLimiterException;
import org.springframework.stereotype.Service;

public interface TokenService {

    void sendOtp(Long userId,String phone,Integer requestTime,Integer userType) throws CommonException, RateLimiterException;

    String verifyOtp(Long userId,String otp,Integer userType) throws Exception;
}
