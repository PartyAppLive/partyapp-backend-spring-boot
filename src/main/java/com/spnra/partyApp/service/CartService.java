package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.request.OfferAddRequest;
import com.spnra.partyApp.dto.request.TicketCreationRequest;
import com.spnra.partyApp.dto.request.TicketOrderRequest;
import com.spnra.partyApp.dto.response.ClubCreationResponse;
import com.spnra.partyApp.dto.response.UserCartResponse;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.exceptions.InValidRequestException;
import org.springframework.stereotype.Service;

public interface CartService {

    UserCartResponse getCart(Long userId) throws Exception;

    UserCartResponse addProduct(OfferAddRequest offerAddRequest, Authorisation authorisation) throws Exception;

    UserCartResponse removeProduct(OfferAddRequest offerAddRequest,Authorisation authorisation) throws Exception;

    Object placeOrder(Long userId);

    Object placeTicketOrder(Long userId, TicketOrderRequest ticketOrderRequest);
}
