package com.spnra.partyApp.service;

import org.springframework.stereotype.Service;

public interface OtpService {

    boolean sendOtp(String phone);
}
