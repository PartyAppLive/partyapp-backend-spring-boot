package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.request.ClubCreationRequest;
import com.spnra.partyApp.dto.request.ClubDetailCreationRequest;
import com.spnra.partyApp.dto.request.ClubInfoUpdateRequest;
import com.spnra.partyApp.dto.request.ClubInterestRequest;
import com.spnra.partyApp.dto.response.ClubCreationResponse;
import com.spnra.partyApp.dto.response.ClubInfoUpdateResponse;
import com.spnra.partyApp.dto.response.ClubInterestResponse;
import com.spnra.partyApp.entity.Authorisation;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

public interface ClubService {

    ClubInfoUpdateResponse updateClubImages(ClubInfoUpdateRequest clubInfoUpdateRequest) throws Exception;

    ClubCreationResponse createClub(ClubCreationRequest clubCreationRequest) throws Exception;

    HashMap<String,String> uploadClubImages(Map<String, MultipartFile> fileMap) throws Exception;

    ClubCreationResponse createClubDetail(ClubDetailCreationRequest clubDetailCreationRequest) throws Exception;

    ClubInterestResponse clubInterest(Long clubId, Authorisation authorisation) throws  Exception;

    ClubInterestResponse removeClubInterest(Long clubId, Authorisation authorisation) throws  Exception;
}
