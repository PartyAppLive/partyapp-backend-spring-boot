package com.spnra.partyApp.service;

import com.amazonaws.services.apigateway.model.UpdateAccountRequest;
import com.spnra.partyApp.dto.request.CancelReservationRequest;
import com.spnra.partyApp.dto.request.ReservationRequest;
import com.spnra.partyApp.dto.request.UpdateReservationRequest;
import com.spnra.partyApp.dto.response.CancelReservationResponse;
import com.spnra.partyApp.dto.response.GetReservationsResponse;
import com.spnra.partyApp.dto.response.PaginatedResponse;
import com.spnra.partyApp.dto.response.ReservationResponse;
import com.spnra.partyApp.entity.Authorisation;
import org.springframework.stereotype.Service;

public interface ReservationService {

    ReservationResponse reserve(ReservationRequest reservationRequest, Authorisation authorisation) throws Exception;

    PaginatedResponse<GetReservationsResponse> getReservations(Authorisation authorisation, Integer page) throws Exception;

    CancelReservationResponse cancelReservation(CancelReservationRequest cancelReservationRequest, Authorisation authorisation) throws Exception;


}
