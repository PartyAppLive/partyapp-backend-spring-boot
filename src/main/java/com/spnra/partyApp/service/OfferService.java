package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.request.ClubCreationRequest;
import com.spnra.partyApp.dto.request.OfferCreationRequest;
import com.spnra.partyApp.dto.request.TicketCreationRequest;
import com.spnra.partyApp.dto.response.ClubCreationResponse;
import com.spnra.partyApp.dto.response.GetOfferResponse;
import com.spnra.partyApp.dto.response.GetTicketResponse;
import com.spnra.partyApp.entity.Authorisation;
import org.springframework.stereotype.Service;

public interface OfferService {

    ClubCreationResponse createOffer(OfferCreationRequest offerCreationRequest) throws Exception;

    GetOfferResponse getOffer(Authorisation authorisation, Long clubId) throws Exception;

    ClubCreationResponse createTicket(TicketCreationRequest ticketCreationRequest) throws Exception;

    GetTicketResponse getTicket(Long clubId) throws Exception;
}
