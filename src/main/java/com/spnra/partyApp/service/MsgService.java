package com.spnra.partyApp.service;

import com.spnra.partyApp.exceptions.CommonException;
import org.springframework.stereotype.Service;

import java.io.IOException;

public interface MsgService {

    Boolean sendSms (String phone,String otp) throws Exception;
}
