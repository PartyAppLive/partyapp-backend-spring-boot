package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.request.EventRequest;
import com.spnra.partyApp.dto.request.EventInterestRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.Authorisation;
import org.springframework.stereotype.Service;


public interface EventService {

    EventCreationResponse createEvent(EventRequest eventRequest) throws Exception;

    EventInterestResponse eventInterest(EventInterestRequest eventInterestRequest, Authorisation authorisation) throws  Exception;

    PaginatedResponse<EventPeopleResponse> peopleInterest(Integer eventId, Authorisation authorisation,Integer page) throws  Exception;

    EventInterestResponse removeEventInterest(EventInterestRequest eventInterestRequest, Authorisation authorisation) throws  Exception;

    PaginatedResponse<GetEventResponse> getAllEvents(Authorisation authorisation,Integer page,Long cityId) throws Exception;

    PaginatedResponse<GetEventResponse> getTodayEvents( Authorisation authorisation, Integer page,Long cityId) throws Exception;

    GetEventSearchResponse searchEvent(String keyWord) throws Exception;


}
