package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.entity.Token;
import com.spnra.partyApp.enums.UserType;
import com.spnra.partyApp.enums.VerificationType;
import com.spnra.partyApp.exceptions.*;
import com.spnra.partyApp.repo.AuthorisationRepo;
import com.spnra.partyApp.repo.TokenRepo;
import com.spnra.partyApp.service.MsgService;
import com.spnra.partyApp.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepo tokenRepo;

    @Autowired
    private AuthorisationRepo authorisationRepo;

    @Autowired
    private MsgService msgService;

    @Override
    public void sendOtp(Long userId,String phone,Integer requestTime,Integer userType) throws CommonException, RateLimiterException {

        Optional<Token> otpToken = tokenRepo.findByUserIdAndUserTypeAndType(userId, userType, VerificationType.OTP.getValue());
        Boolean sendSms;
        if (ObjectUtils.isEmpty(otpToken)) {
            String otpString = String.valueOf((int) (Math.random() * 9000) + 1000);
            try {
                sendSms = msgService.sendSms(phone, otpString);
            } catch (Exception e) {
                log.error("Some error occurred while sending otp verification sms msg");
                throw new CommonException("Some error occurred while sending sms");
            }
            if (sendSms) {
                Token token = new Token().toBuilder()
                        .otp(otpString)
                        .userId(userId)
                        .expiryTime(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)))
                        .type(VerificationType.OTP.getValue())
                        .userType(UserType.NORMAL.getValue())
                        .status(Boolean.TRUE)
                        .attempt(1)
                        .build();
                tokenRepo.save(token);
            }
        }
        else if(otpToken.get().getExpiryTime().before(new Date(System.currentTimeMillis()))||otpToken.get().getStatus().equals(Boolean.FALSE)){
            String otpString = String.valueOf((int) (Math.random() * 9000) + 1000);
            try {
                sendSms = msgService.sendSms(phone, otpString);
            } catch (Exception e) {
                log.error("Some error occurred while sending otp verification sms msg");
                throw new CommonException("Some error occurred while sending sms");
            }
            if (sendSms) {
                otpToken.get().setExpiryTime(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)));
                otpToken.get().setOtp(otpString);
                otpToken.get().setAttempt(otpToken.get().getAttempt() + 1);
                otpToken.get().setStatus(Boolean.TRUE);
                tokenRepo.save(otpToken.get());
            }
        }
        else if(otpToken.get().getExpiryTime().after(new Date(System.currentTimeMillis()))&&otpToken.get().getStatus().equals(Boolean.TRUE)){
            if (otpToken.get().getUpdatedAt().before(new Date(System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(requestTime)))) {
                String otp = otpToken.get().getOtp();
                try {
                    sendSms = msgService.sendSms(phone,otp);
                } catch (Exception e) {
                    log.error("Some error occurred while sending otp verification sms msg");
                    throw new CommonException("Some error occurred while sending sms");
                }
                if (sendSms) {
                    otpToken.get().setAttempt(otpToken.get().getAttempt() + 1);
                    otpToken.get().setStatus(Boolean.TRUE);
                    tokenRepo.save(otpToken.get());
                }
            }else {
                throw new RateLimiterException("PLease try after 30 seconds");
            }
        }
    }

    @Override
    public String verifyOtp(Long userId, String otp,Integer userType) throws Exception {
        Optional<Token> otpToken = tokenRepo.findByUserIdAndUserTypeAndTypeAndStatus(userId, userType, VerificationType.OTP.getValue(), Boolean.TRUE);
        if (ObjectUtils.isEmpty(otpToken)) {
            throw new InValidRequestException("Not a valid request");
        }
        String eId;
        if (otpToken.get().getExpiryTime().after(new Date(System.currentTimeMillis()))) {
            if (otpToken.get().getOtp().equals(otp)) {
                //call device details function
                Long records = authorisationRepo.deleteAllByUserId(userId);
                eId = UUID.randomUUID().toString();
                Authorisation authorisation = new Authorisation().toBuilder()
                        .userId(userId)
                        .userType(UserType.NORMAL.getValue())
                        .expiryTime(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(2000)))
                        .uuid(eId)
                        .build();
                authorisationRepo.save(authorisation);
                otpToken.get().setStatus(Boolean.FALSE);
                tokenRepo.save(otpToken.get());
            } else {
                throw new InCorrectOtpRequest("Invalid otp");
            }
        } else {
            throw new OtpExpiredException("Otp has Expired");
        }
        return eId;
    }


}
