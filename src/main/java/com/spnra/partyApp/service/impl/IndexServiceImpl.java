package com.spnra.partyApp.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spnra.partyApp.constants.PartyAppConstants;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.*;
import com.spnra.partyApp.exceptions.CommonException;
import com.spnra.partyApp.exceptions.ContentNotFoundException;
import com.spnra.partyApp.exceptions.InValidRequestException;
import com.spnra.partyApp.repo.*;
import com.spnra.partyApp.service.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private ClubRepo clubRepo;

    @Autowired
    private DetailRepo detailRepo;

    @Autowired
    private ClubInterestRepo clubInterestRepo;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public PaginatedResponse<HomePageResponse> getAllClubs(Authorisation authorisation,Integer page,Long cityId) throws Exception {
        long totalCount;
        long offset = page * 5;

        String dayOfWeek = new SimpleDateFormat("EEEE").format(new Date()).toLowerCase(Locale.ROOT);
        List<HomePageResponse.HomePageResponseModel> homePageResponseModelList = new ArrayList<>();
        List<Map<String, Object>> clubList;
        try {
            clubList = clubRepo.fetchAllClubsBasedOnCityAndStatus(cityId, offset, 5);
            totalCount = clubRepo.getAllClubCountByCityAndStatus(cityId);
        }
        catch(Exception e){
            log.error("Error occurred while getting all clubs with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        if(ObjectUtils.isEmpty(clubList)){
            throw new ContentNotFoundException("No Records to display");
        }
        if(totalCount == 0){
            return new PaginatedResponse<>(page,0,totalCount,5,new HomePageResponse(),HomePageResponse.class);
        }
        clubList.stream().forEach(e->{
            Map<String, String> timings = null;
            try {
                timings = objectMapper.readValue((String)e.get("timings"), Map.class);
            } catch (JsonProcessingException jsonProcessingException) {
                jsonProcessingException.printStackTrace();
            }
            HomePageResponse.HomePageResponseModel homePageResponseModel = new HomePageResponse.HomePageResponseModel().toBuilder()
                    .id(Long.valueOf(e.get("id").toString()))
                    .name(e.get("name").toString())
                    .discount((e.get("discount")==null)?null:e.get("discount").toString())
                    .latitude((e.get("latitude")==null)?null:e.get("latitude").toString())
                    .longitude((e.get("longitude")==null)?null:e.get("longitude").toString())
                    .timing(timings.get(dayOfWeek))
                    .image(e.get("image").toString())
                    .area(e.get("area").toString())
                    .directionLink(e.get("address_link").toString())
                    .build();
            homePageResponseModelList.add(homePageResponseModel);
        });

        HomePageResponse homePageResponse;
        homePageResponse = new HomePageResponse().toBuilder()
                .homePageResponseModelList(homePageResponseModelList)
                .build();
        return new PaginatedResponse<>(page,homePageResponseModelList.size(),totalCount,5, homePageResponse,HomePageResponse.class);
    }

    @Override
    public PaginatedResponse<HomePageResponse> getAllOfferClubs(Authorisation authorisation, Integer page, Long cityId) throws Exception {
        long totalCount;
        long offset = page * 5;

        String dayOfWeek = new SimpleDateFormat("EEEE").format(new Date()).toLowerCase(Locale.ROOT);
        List<HomePageResponse.HomePageResponseModel> homePageResponseModelList = new ArrayList<>();
        List<Map<String, Object>> clubOfferList ;
        try {
            clubOfferList = clubRepo.fetchAllClubsBasedOnCityAndStatusAndDiscount(cityId, offset, 5);
            totalCount = clubRepo.getAllClubCountByCityAndStatusAndDiscount(cityId);
        }
        catch(Exception e){
            log.error("Error occurred while getting all clubs with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        if(ObjectUtils.isEmpty(clubOfferList)){
            throw new ContentNotFoundException("No Records to display");
        }
        if(totalCount == 0){
            return new PaginatedResponse<>(page,0,totalCount,5,new HomePageResponse(),HomePageResponse.class);
        }

        clubOfferList.stream().forEach(e->{
            Map<String, String> timings = null;
            try {
                timings = objectMapper.readValue((String)e.get("timings"), Map.class);
            } catch (JsonProcessingException jsonProcessingException) {
                jsonProcessingException.printStackTrace();
            }
            HomePageResponse.HomePageResponseModel homePageResponseModel = new HomePageResponse.HomePageResponseModel().toBuilder()
                    .id(Long.valueOf(e.get("id").toString()))
                    .name(e.get("name").toString())
                    .discount((e.get("discount")==null)?null:e.get("discount").toString())
                    .latitude((e.get("latitude")==null)?null:e.get("latitude").toString())
                    .longitude((e.get("longitude")==null)?null:e.get("longitude").toString())
                    .image(e.get("image").toString())
                    .directionLink(e.get("address_link").toString())
                    .area(e.get("area").toString())
                    .timing(timings.get(dayOfWeek))
                    .build();
            homePageResponseModelList.add(homePageResponseModel);
        });

        HomePageResponse homePageResponse;
        homePageResponse = new HomePageResponse().toBuilder()
                .homePageResponseModelList(homePageResponseModelList)
                .build();
        return new PaginatedResponse<>(page,homePageResponseModelList.size(),totalCount,5, homePageResponse,HomePageResponse.class);
    }

    @Override
    public PdpPageResponse getClub(Authorisation authorisation, Long clubId) throws Exception {
        PdpPageResponse pdpPageResponse ;
        Optional<Detail> detail;
        Optional<Club> club;
        try {
            club = clubRepo.findByIdAndStatus(clubId, true);
            if (ObjectUtils.isEmpty(club)) {
                throw new InValidRequestException("Bad Request");
            }
            detail = detailRepo.findById(clubId);
            if (ObjectUtils.isEmpty(detail)) {
                throw new CommonException("Something went wrong");
            }
        }
        catch(Exception e){
            log.error("Error occurred while getting club with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        Map<String, String> day_special = objectMapper.readValue(detail.get().getDaySpecial(), Map.class);
        Map<String, String> timings = objectMapper.readValue(detail.get().getTimings(), Map.class);

        String dayOfWeek = new SimpleDateFormat("EEEE").format(new Date()).toLowerCase(Locale.ROOT);

        pdpPageResponse = new PdpPageResponse().toBuilder()
                .clubId(club.get().getId())
                .description(detail.get().getDescription())
                .mobileNumber(club.get().getMobileNumber())
                .address(club.get().getAddress())
                .addressLink(club.get().getAddressLink())
                .beerText(detail.get().getBeerText())
                .beerRate(detail.get().getBeerRate())
                .kitchenText(detail.get().getKitchenText())
                .kitchenRate(detail.get().getKitchenRate())
                .photos(detail.get().getPhotos())
                .daySpecial(day_special.get(dayOfWeek))
                .timings(timings.get(dayOfWeek))
                .favourite(Boolean.FALSE)
                .menu(detail.get().getMenu())
                .valet(detail.get().getValet())
                .build();
        try {
            Optional<ClubInterest> clubInterest = clubInterestRepo.findAllByClubIdAndUserId(club.get().getId(), authorisation.getUserId());
            if (!ObjectUtils.isEmpty(clubInterest)) {
                pdpPageResponse.setFavourite(Boolean.TRUE);
            }
        }
        catch (Exception e){
            log.error("Error occurred while getting club favourite info with exception = {}",e.toString());
        }
        return pdpPageResponse;
    }

    @Override
    public PaginatedResponse<HomePageResponse> getAllFavouriteClubs(Authorisation authorisation,Integer page,Long cityId) throws Exception {

        long totalCount;
        long offset = page * 5;

        List<Map<String, Object>> clubInterestList ;

        try{
        clubInterestList = clubInterestRepo.fetchAllFavouriteClubsBasedOnUserAndStatus(authorisation.getUserId(),cityId,offset,5);
        totalCount = clubInterestRepo.getAllFavouriteClubsCountByCityAndStatus(authorisation.getUserId(),cityId);
        }
        catch(Exception e){
            log.error("Error occurred while getting all favourite club with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }

        if(ObjectUtils.isEmpty(clubInterestList)){
            throw new ContentNotFoundException("No Records to display");
        }
        if(totalCount == 0){
            return new PaginatedResponse<>(page,0,totalCount,5,new HomePageResponse(),HomePageResponse.class);
        }

        HashSet<Long> idList = new HashSet<>();
        clubInterestList.stream().forEach(e->{
            idList.add(Long.valueOf(e.get("club_id").toString()));
        });
        List<Club> clubList = clubRepo.findByIdInAndStatus(idList,true);
        HomePageResponse homePageResponse;
        List<HomePageResponse.HomePageResponseModel> homePageResponseModelList = new ArrayList<>();
        for(Club x : clubList) {
            HomePageResponse.HomePageResponseModel homePageResponseModel = new HomePageResponse.HomePageResponseModel().toBuilder()
                    .id(x.getId())
                    .image(x.getImage())
                    .directionLink(x.getAddressLink())
                    .name(x.getName())
                    .discount(x.getDiscount())
                    .build();
            homePageResponseModelList.add(homePageResponseModel);
        }
        homePageResponse = new HomePageResponse().toBuilder()
                .homePageResponseModelList(homePageResponseModelList)
                .build();
        return new PaginatedResponse<>(page,homePageResponseModelList.size(),totalCount,5, homePageResponse,HomePageResponse.class);
    }

    @Override
    public GetClubSearchResponse searchClub(Long cityId, String keyWord) throws Exception {
        if (ObjectUtils.isEmpty(cityId)||ObjectUtils.isEmpty(keyWord)) {
            throw new InValidRequestException("Bad Request");
        }
        List<GetClubSearchResponse.SearchResponseModel> homePageResponseModelList = new ArrayList<>();
        List<Map<String, Object>> clubOfferList ;
        try {
            clubOfferList = clubRepo.searchClubBasedOnKeyword(cityId,keyWord);
        }
        catch(Exception e){
            log.error("Error occurred while getting all clubs with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        if(ObjectUtils.isEmpty(clubOfferList)){
            throw new ContentNotFoundException("No Records to display");
        }
        clubOfferList.stream().forEach(e->{
            GetClubSearchResponse.SearchResponseModel searchResponseModel = new GetClubSearchResponse.SearchResponseModel().toBuilder()
                    .id(Long.valueOf(e.get("id").toString()))
                    .name(e.get("name").toString())
                    .image(e.get("image").toString())
                    .build();
            homePageResponseModelList.add(searchResponseModel);
        });

        GetClubSearchResponse searchResponse;
        searchResponse = new GetClubSearchResponse().toBuilder()
                .searchClubResponseModelList(homePageResponseModelList)
                .build();
        return searchResponse;
    }

    @Override
    public GetLocationsResponse getLocations() {
        List<String> idList = new ArrayList<>();
        idList.add("Siliguri");
        idList.add("Bengaluru");
        idList.add("Goa");
        GetLocationsResponse getLocationsResponse =  new GetLocationsResponse().toBuilder()
                .locationsList(idList)
                .build();
        return getLocationsResponse;

    }

    @Override
    public ContactUsResponse getContactUs()  {
        String msg = PartyAppConstants.contactUsMsg;
        ContactUsResponse contactUsResponse = new ContactUsResponse().toBuilder()
                .msg(msg)
                .build();
        return  contactUsResponse;
    }
}
