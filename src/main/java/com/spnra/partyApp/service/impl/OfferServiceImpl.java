package com.spnra.partyApp.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spnra.partyApp.dto.request.OfferCreationRequest;
import com.spnra.partyApp.dto.request.TicketCreationRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.entity.Club;
import com.spnra.partyApp.entity.Offer;
import com.spnra.partyApp.entity.Ticket;
import com.spnra.partyApp.exceptions.ContentNotFoundException;
import com.spnra.partyApp.exceptions.InValidRequestException;
import com.spnra.partyApp.model.DayOfWeekModel;
import com.spnra.partyApp.repo.ClubRepo;
import com.spnra.partyApp.repo.OfferRepo;
import com.spnra.partyApp.repo.TicketRepo;
import com.spnra.partyApp.service.OfferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;

@Slf4j
@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferRepo offerRepo;

    @Autowired
    private TicketRepo ticketRepo;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClubRepo clubRepo;


    @Override
    public ClubCreationResponse createOffer(OfferCreationRequest offerCreationRequest) throws Exception {
        if(ObjectUtils.isEmpty(offerCreationRequest)){
            throw new InValidRequestException("Bad Request");
        }

        ClubCreationResponse clubCreationResponse;
        try {
            Offer offer = new Offer().toBuilder()
                    .clubId(offerCreationRequest.getClubId())
                    .desc(offerCreationRequest.getDesc())
                    .image(offerCreationRequest.getImage())
                    .timing(objectMapper.writeValueAsString(offerCreationRequest.getDayOfWeekModel()))
                    .inventory(offerCreationRequest.getInventory())
                    .soldUnit(0)
                    .startDate(offerCreationRequest.getStartDate())
                    .endDate(offerCreationRequest.getEndDate())
                    .originalPrice(offerCreationRequest.getOriginalPrice())
                    .discountedPrice(offerCreationRequest.getDiscountedPrice())
                    .discount(offerCreationRequest.getDiscountPercentage())
                    .status(Boolean.TRUE)
                    .bestSeller(Boolean.TRUE)
                    .offerType(offerCreationRequest.getOfferType())
                    .drinksInclusions(objectMapper.writeValueAsString(offerCreationRequest.getDrinkInclusionModel()))
                    .foodInclusions(offerCreationRequest.getFoodInclusions().toString())
                    .terms(offerCreationRequest.getTerms())
                    .build();
            try {
                offerRepo.save(offer);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while adding new offer with exception = {}",e.toString());
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.FALSE)
                    .build();
        }
        return  clubCreationResponse;
    }

    @Override
    public GetOfferResponse getOffer(Authorisation authorisation, Long clubId) throws Exception {
        if(ObjectUtils.isEmpty(clubId)){
            throw new InValidRequestException("Bad Request");
        }
        Optional<Club> club = clubRepo.findById(clubId);
        if(ObjectUtils.isEmpty(club)){
            throw new InValidRequestException("Invalid Request");
        }
        List<GetOfferResponse.GetOfferResponseModel> getOfferResponseModelList = new ArrayList<>();
        List<Offer> offerList = offerRepo.findAllByClubIdAndStatusAndEndDateAfter(clubId,Boolean.TRUE,new Date());
        if(ObjectUtils.isEmpty(offerList)){
            log.info("No offers found while checking for club Id = ", + clubId);
            throw new ContentNotFoundException("No Records to display");
        }

        //get cart if no cart present then skip
        //if present check for offerId and quantity.
        for(Offer offer:offerList){
            //if hashmap contains this offerId -> then  get quantity else 0;
            GetOfferResponse.GetOfferResponseModel offerResponseBuilder = new GetOfferResponse.GetOfferResponseModel().toBuilder()
                    .offerId(offer.getId())
                    .currentQuantity(0)
                    .clubId(clubId)
                    .originalPrice(offer.getOriginalPrice())
                    .discountedPrice(offer.getDiscountedPrice())
                    .discountPercentage(offer.getDiscount())
                    .timings(objectMapper.readValue(offer.getTiming(), DayOfWeekModel.class))
                    .desc(offer.getDesc())
                    .image(offer.getImage())
                    .drinksInclusions(objectMapper.readValue(offer.getDrinksInclusions(), OfferCreationRequest.DrinkInclusionModel.class))
                    .foodInclusions(offer.getFoodInclusions())
                    .terms(offer.getTerms())
                    .build();
            //get cart if no cart present then skip
            getOfferResponseModelList.add(offerResponseBuilder);
        }

        GetOfferResponse getOfferResponse;
        getOfferResponse = new GetOfferResponse().toBuilder()
                .offerResponseModelList(getOfferResponseModelList)
                .build();

        return getOfferResponse;
    }

    @Override
    public ClubCreationResponse createTicket(TicketCreationRequest ticketCreationRequest) throws Exception {
        if(ObjectUtils.isEmpty(ticketCreationRequest)){
            throw new InValidRequestException("Bad Request");
        }

        ClubCreationResponse clubCreationResponse;
        try {
            Ticket ticket = new Ticket().toBuilder()
                    .eventId(ticketCreationRequest.getEventId())
                    .name(ticketCreationRequest.getName())
                    .image(ticketCreationRequest.getImage())
                    .inventory(ticketCreationRequest.getInventory())
                    .soldUnit(0)
                    .startDate(ticketCreationRequest.getStartDate())
                    .endDate(ticketCreationRequest.getEndDate())
                    .price(ticketCreationRequest.getPrice())
                    .status(Boolean.TRUE)
                    .info(ticketCreationRequest.getInfo())
                    .maxQuantity(ticketCreationRequest.getMaxQuantity())
                    .terms(ticketCreationRequest.getTerms())
                    .build();
            ticketRepo.save(ticket);
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while creating new ticket offer with exception = {}",e.toString());
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.FALSE)
                    .build();
        }
        return  clubCreationResponse;
    }

    @Override
    public GetTicketResponse getTicket(Long eventId) throws Exception {
        if(ObjectUtils.isEmpty(eventId)){
            throw new InValidRequestException("Bad Request");
        }

        List<GetTicketResponse.GetTicketResponseModel> getTicketResponseModelList = new ArrayList<>();
        Optional<List<Ticket>> ticketList = ticketRepo.findByEventId(eventId);
        if(ObjectUtils.isEmpty(ticketList)){
            log.info("No ticket found while checking for event Id = ", + eventId);
            throw new ContentNotFoundException("No Records to display");
        }
        List<Ticket> newTicketList =  ticketList.get();

        for(Ticket ticket:newTicketList){
            GetTicketResponse.GetTicketResponseModel ticketResponseModel = new GetTicketResponse.GetTicketResponseModel().toBuilder()
                    .phaseId(ticket.getId())
                    .eventId(eventId)
                    .name(ticket.getName())
                    .price(ticket.getPrice())
                    .image(ticket.getImage())
                    .terms(ticket.getTerms())
                    .info(ticket.getInfo())
                    .quantityLeft(ticket.getInventory()-ticket.getSoldUnit())
                    .maxQuantity(ticket.getMaxQuantity())
                    .status(ticket.getStatus())
                    .build();
            getTicketResponseModelList.add(ticketResponseModel);
        }

        GetTicketResponse getTicketResponse;
        getTicketResponse = new GetTicketResponse().toBuilder()
                .ticketResponseModelList(getTicketResponseModelList)
                .build();

        return getTicketResponse;
    }
}
