package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.constants.PartyAppConstants;
import com.spnra.partyApp.dto.response.UserResponse;
import com.spnra.partyApp.dto.request.*;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.*;
import com.spnra.partyApp.enums.*;
import com.spnra.partyApp.exceptions.*;
import com.spnra.partyApp.model.InternalUserResponse;
import com.spnra.partyApp.model.S3DocUploader;
import com.spnra.partyApp.repo.*;
import com.spnra.partyApp.service.TokenService;
import com.spnra.partyApp.service.UserService;
import com.spnra.partyApp.utils.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;


@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private S3DocUploader s3DocUploader;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AuthorisationRepo authorisationRepo;

    @Value(("${s3.key}"))
    private String s3Key;

    @Transactional(rollbackFor=Exception.class)
    @Override
    public UserResponse createUser(UserRequest userRequest) throws Exception {
        log.info("Received User Create Request: {}", userRequest);
        if (ObjectUtils.isEmpty(userRequest)) {
            return null;
        }
        if (ObjectUtils.isEmpty(userRequest.getName()) || ObjectUtils.isEmpty(userRequest.getPhone())) {
            log.error("Incomplete details while trying to signup");
            throw new InCompleteDetailsException("Missing details");
        }

        Boolean check = AppUtils.phoneNumberValidate(userRequest.getPhone());
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }

        Optional<User> user = userRepo.findByPhoneNumber(userRequest.getPhone());
        if (!ObjectUtils.isEmpty(user)) {
            throw new UserAlreadyExistException("User already exist for this phone number");
        }
        UserResponse userResponse;
        try {
            User userCreate = new User()
                    .toBuilder()
                    .name(userRequest.getName())
                    .phoneNumber(userRequest.getPhone())
                    .signUpStatus(SignUpState.OTP_SENT.getValue())
                    .userType("Normal")
                    .emailVerified(Boolean.FALSE)
                    .status(Boolean.TRUE)
                    .cityId(City.SILIGURI.getValue())
                    .shareToken(generateToken())
                    .build();
            userRepo.save(userCreate);
        } catch (Exception e) {
            log.error("Error occurred while creating new user : {} with exception :{}", userRequest.getPhone(), e.toString());
            throw new CommonException("Some Error Occurred");
        }
        log.info("Successfully created user with phone number: {}", userRequest.getPhone());
        InternalUserResponse signUpUser = getInternalUser(null,userRequest.getPhone());
        try {
            signIn(signUpUser.getPhone());
        } catch (Exception e) {
            log.error("Some error occurred while signing in with this number: {} with exception:{}", userRequest.getPhone(), e.toString());
            throw new CommonException("Some Error Occurred");
        }
        try {
            userResponse = new UserResponse().toBuilder()
                    .name(signUpUser.getName())
                    .phone(signUpUser.getPhone())
                    .status(signUpUser.getStatus())
                    .userType(signUpUser.getUserType())
                    .image(signUpUser.getImage())
                    .shareToken(signUpUser.getShareToken())
                    .email(signUpUser.getEmail())
                    .cityId(signUpUser.getCityId())
                    .emailVerified(signUpUser.getEmailVerified())
                    .signupStatus(signUpUser.getSignUpStatus())
                    .build();
        } catch (Exception e) {
            log.error("Some error occurred while fetching details for this number: {} with exception:{}", userRequest.getPhone(), e.toString());
            throw new CommonException("Some Error Occurred");
        }
        return userResponse;
    }

    @Override
    public UserResponse getUser(Long userId, String phone) throws Exception {
        if (ObjectUtils.isEmpty(userId) && ObjectUtils.isEmpty(phone)) {
            log.error("Invalid Request to get user");
            throw new InValidRequestException("Invalid Request to get exception");
        }
        UserResponse userResponse;
        Optional<User> user;
        if (!ObjectUtils.isEmpty(userId)) {
            user = userRepo.findById(userId);
        } else {
            user = userRepo.findByPhoneNumber(phone);
        }
        if (ObjectUtils.isEmpty(user)) {
            throw new UserNotFoundException("User Not Found");
        }
        userResponse = new UserResponse().toBuilder()
                .name(user.get().getName())
                .phone(user.get().getPhoneNumber())
                .status(user.get().getStatus())
                .userType(user.get().getUserType())
                .image(user.get().getImage())
                .shareToken(user.get().getShareToken())
                .email(user.get().getEmail())
                .cityId(user.get().getCityId())
                .emailVerified(user.get().getEmailVerified())
                .signupStatus(user.get().getSignUpStatus())
                .build();
        return userResponse;
    }

    @Override
    public InternalUserResponse getInternalUser(Long userId, String phone) throws Exception {
        if (ObjectUtils.isEmpty(userId) && ObjectUtils.isEmpty(phone)) {
            log.error("Invalid Request to get user");
            throw new InValidRequestException("Invalid Request");
        }
        InternalUserResponse userResponse;
        Optional<User> user;
        if (!ObjectUtils.isEmpty(userId)) {
            user = userRepo.findById(userId);
        } else {
            user = userRepo.findByPhoneNumber(phone);
        }
        if (ObjectUtils.isEmpty(user)) {
            throw new UserNotFoundException("User Not Found");
        }
        userResponse = new InternalUserResponse().toBuilder()
                .id(user.get().getId())
                .name(user.get().getName())
                .phone(user.get().getPhoneNumber())
                .status(user.get().getStatus())
                .userType(user.get().getUserType())
                .image(user.get().getImage())
                .shareToken(user.get().getShareToken())
                .email(user.get().getEmail())
                .cityId(user.get().getCityId())
                .emailVerified(user.get().getEmailVerified())
                .signUpStatus(user.get().getSignUpStatus())
                .build();
        return userResponse;
    }

    @Override
    public SignInResponse signIn(String phone) throws Exception {
        SignInResponse signInResponse ;
        log.info("Received User Sign In Request: {}", phone);
        if (ObjectUtils.isEmpty(phone)) {
            throw new InValidRequestException("Missing details");
        }
        Boolean check = AppUtils.phoneNumberValidate(phone);
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }
        InternalUserResponse user;
        try {
            user = getInternalUser(null,phone);
        }
        catch (UserNotFoundException e) {
            log.error("No user Found with phone number", phone);
            throw new UserNotFoundException("User Not Found");
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching user details = {}", e.toString(), phone);
            throw new CommonException("Some Error Occurred");
        }
        tokenService.sendOtp(user.getId(),user.getPhone(),10,UserType.NORMAL.getValue());
        signInResponse = new SignInResponse().toBuilder()
                .otpSent(Boolean.TRUE.booleanValue())
                .build();
        return signInResponse;
    }

    @Override
    public SignOutResponse signOut(Long id) throws Exception {
        SignOutResponse signOutResponse;
        if (ObjectUtils.isEmpty(id)) {
            throw new InValidRequestException("Missing id");
        }
        try {
            authorisationRepo.deleteById(id);
            signOutResponse = new SignOutResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        } catch (Exception e) {
            log.error("some error occurred while signing out with exception = {}", e.toString());
            throw new CommonException("Some error occurred");
        }
        return signOutResponse;
    }

    @Override
    public OtpVerificationResponse otpVerify(OtpVerificationRequest otpVerificationRequest) throws Exception {
        OtpVerificationResponse otpVerificationResponse;
        log.info("Received User Otp Verification Request: {}", otpVerificationRequest.getPhone());
        if (ObjectUtils.isEmpty(otpVerificationRequest.getPhone())||ObjectUtils.isEmpty(otpVerificationRequest.getOtp())) {
            throw new InValidRequestException("Missing details");
        }

        Boolean check = AppUtils.phoneNumberValidate(otpVerificationRequest.getPhone());
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }
        InternalUserResponse userResponse;
        try {
            userResponse = getInternalUser(null,otpVerificationRequest.getPhone());
        }
        catch (UserNotFoundException e) {
            log.error("No user Found with phone number", otpVerificationRequest.getPhone());
            throw new UserNotFoundException("User Not Found");
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching user details = {}", e.toString(), otpVerificationRequest.getPhone());
            throw new CommonException("Some Error Occurred");
        }
        String eId = tokenService.verifyOtp(userResponse.getId(),otpVerificationRequest.getOtp(),UserType.NORMAL.getValue());
        verifyUser(userResponse.getId());
        otpVerificationResponse = new OtpVerificationResponse().toBuilder()
                .otpVerified(Boolean.TRUE)
                .authorisation(eId)
                .build();
        return otpVerificationResponse;
    }

    @Override
    public ClubInfoUpdateResponse selectCity(CitySelectRequest request, Authorisation authorisation) throws Exception {
        ClubInfoUpdateResponse clubInfoUpdateResponse ;
        log.info("Received User city update Request for userId: {}", authorisation.getUserId());
        if (ObjectUtils.isEmpty(request.getCity())) {
            throw new InValidRequestException("Missing details");
        }
        Optional<User> user;
        try {
            //because we will need to update the user data and again convert internal user response to user.
            user = userRepo.findById(authorisation.getUserId());
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching user details with exception = {}", e.toString());
            throw new CommonException("Some Error Occurred");
        }
        try{
            user.get().setCityId(City.valueOf(request.getCity().toUpperCase(Locale.ROOT)).getType());
            userRepo.save(user.get());
        }
        catch (Exception e) {
            log.error("Some Exception occurred while updating user details with exception = {}", e.toString());
            throw new CommonException("Some Error Occurred");
        }
        clubInfoUpdateResponse = new ClubInfoUpdateResponse().toBuilder()
                .success(Boolean.TRUE)
                .build();
        return clubInfoUpdateResponse;
    }

    @Override
    public ImageUploadResponse dpUpload(Long userId, Map<String, MultipartFile> fileMap) throws Exception {
        Optional<User> user = userRepo.findById(userId);
        if (ObjectUtils.isEmpty(user)) {
            throw new UserNotFoundException("USer not found");
        }
        ImageUploadResponse imageUploadResponse = null;
        for (Map.Entry m : fileMap.entrySet()) {
            File doc = s3DocUploader.convertMultiPartToFile((MultipartFile) m.getValue());
            String file_name;
            try {
                file_name = "user_" + userId;
                String extension = ((MultipartFile) m.getValue()).getContentType();

                String fileExtension = FilenameUtils.getExtension(((MultipartFile) m.getValue()).getOriginalFilename());
                if (!extension.equalsIgnoreCase("image/heic") && !extension.equalsIgnoreCase("image/png") && !extension.equalsIgnoreCase("image/jpeg")) {
                    throw new InCorrectFileTypeException("Incorrect File type");
                }
                if (!fileExtension.equalsIgnoreCase("heic") && !fileExtension.equalsIgnoreCase("png") && !fileExtension.equalsIgnoreCase("jpeg")) {
                    throw new InCorrectFileTypeException("Incorrect File type");
                }
            } catch (Exception e) {
                throw new CommonException("Some Error Occurred");
            }
            try {
                OutputStream oos = new FileOutputStream("/tmp/" + file_name);
                byte[] buf = new byte[8192];
                InputStream is = new FileInputStream(doc);

                int c = 0;
                while ((c = is.read(buf, 0, buf.length)) > 0) {
                    oos.write(buf, 0, c);
                    oos.flush();
                }
                s3DocUploader.uploadFileToS3("/tmp/" + file_name, s3Key + "/" + file_name);
                imageUploadResponse = new ImageUploadResponse().toBuilder()
                        .url(PartyAppConstants.S3_DOC_URL + file_name)
                        .build();
                user.get().setImage(PartyAppConstants.S3_DOC_URL + file_name);
                userRepo.save(user.get());
                log.info("Successful execution and uploading of user Documents: {}",
                        new File(file_name));
            } catch (Exception e) {
                log.info("Error in uploading user Documents with Exception {}", e.toString());
                throw new Exception("Some Error Occurred");
            }
            doc.delete();
        }
        return imageUploadResponse;
    }

    @Override
    public SignInResponse otpResend(String phone) throws Exception {
        SignInResponse signInResponse = null;
        if (ObjectUtils.isEmpty(phone)) {
            return null;
        }
        Boolean check = AppUtils.phoneNumberValidate(phone);
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }
        log.info("Received User Resend Otp Request: {}", phone);
        InternalUserResponse userResponse;
        try {
            userResponse = getInternalUser(null,phone);
        }
        catch (UserNotFoundException e) {
            log.error("No user Found with phone number",phone);
            throw new UserNotFoundException("User Not Found");
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching user details = {}", e.toString(),phone);
            throw new CommonException("Some Error Occurred");
        }
        tokenService.sendOtp(userResponse.getId(),userResponse.getPhone(),30,UserType.NORMAL.getValue());
        signInResponse = new SignInResponse().toBuilder()
                .otpSent(Boolean.TRUE.booleanValue())
                .build();
        return signInResponse;
    }

    @Override
    public UserProfileUpdateResponse profileUpdate(UserProfileUpdateRequest userProfileUpdateRequest, Authorisation authorisation) throws Exception {
        UserProfileUpdateResponse userProfileUpdateResponse;
        if (ObjectUtils.isEmpty(userProfileUpdateRequest)) {
            throw new InValidRequestException("Invalid Request");
        }
        if (ObjectUtils.isEmpty(userProfileUpdateRequest.getEmail()) && ObjectUtils.isEmpty(userProfileUpdateRequest.getName())) {
            throw new InCompleteDetailsException("Missing Details");
        }
        Optional<User> user = userRepo.findById(authorisation.getUserId());
        if (ObjectUtils.isEmpty(user)) {
            throw new UserNotFoundException("User doesn't exist");
        }
        try {
            user.get().setEmail(userProfileUpdateRequest.getEmail());
            user.get().setName(userProfileUpdateRequest.getName());
            userRepo.save(user.get());
            userProfileUpdateResponse = new UserProfileUpdateResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        } catch (Exception e) {
            log.error("Some error occurred while updating user profile");
            throw new CommonException("Some error occurred");
        }
        return userProfileUpdateResponse;
    }

    @Async
    public void verifyUser(Long userId) {
        try {
            Optional<User> user = userRepo.findById(userId);
            user.get().setSignUpStatus(SignUpState.OTP_VERIFIED.getValue());
            userRepo.save(user.get());
        } catch (Exception e) {
            log.error("Error occurred while updating user sign up status with expception = {}", e.toString());
        }
    }

    public String generateToken() {
        boolean flag = true;
        String shareToken = "";
        while (flag) {
            shareToken = RandomStringUtils.randomAlphanumeric(6).toLowerCase();
            Optional<User> validateShareToken = userRepo.findByShareToken(shareToken);
            if (!validateShareToken.isPresent()) {
                flag = false;
            }
        }
        return shareToken;
    }
}
