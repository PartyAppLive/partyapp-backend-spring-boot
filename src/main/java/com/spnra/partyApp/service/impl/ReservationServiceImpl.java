package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.constants.PartyAppConstants;
import com.spnra.partyApp.dto.request.CancelReservationRequest;
import com.spnra.partyApp.dto.request.ReservationRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.*;
import com.spnra.partyApp.enums.ReservationStatus;
import com.spnra.partyApp.exceptions.*;
import com.spnra.partyApp.repo.*;
import com.spnra.partyApp.service.ReservationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Time;
import java.util.*;

@Slf4j
@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ClubRepo clubRepo;

    @Autowired
    private ReservationRepo reservationRepo;

    @Autowired
    private DetailRepo detailRepo;


    @Override
    public ReservationResponse reserve(ReservationRequest reservationRequest,Authorisation authorisation) throws Exception{
        ReservationResponse reservationResponse = null;
        if(ObjectUtils.isEmpty(reservationRequest.getBookedDate())||ObjectUtils.isEmpty(reservationRequest.getBookedTime())||ObjectUtils.isEmpty(reservationRequest.getPerson())||ObjectUtils.isEmpty(reservationRequest.getClubId())){
            throw new InCompleteDetailsException("Missing details");
        }

        Optional<Club> club = clubRepo.findByIdAndStatus(reservationRequest.getClubId(),true);
        if(ObjectUtils.isEmpty(club)){
            throw new InValidRequestException("Bad Request");
        }

        Optional<Detail> detail = detailRepo.findByClubId(reservationRequest.getClubId());
        if(ObjectUtils.isEmpty(detail)){
            throw new InValidRequestException("Bad Request");
        }

        if(!detail.get().getReservation()){
            throw new InValidRequestException("Club doesn't accept reservations");
        }

        try {
            Reservation reservation = new Reservation().toBuilder()
                    .clubId(club.get().getId())
                    .clubName(club.get().getName())
                    .bookedDate(reservationRequest.getBookedDate())
                    .person(reservationRequest.getPerson())
                    .userId(authorisation.getUserId())
                    .bookedTime(reservationRequest.getBookedTime())
                    .status(ReservationStatus.CONFIRMATION_PENDING.getValue())
                    .build();
            //send sms to user
            //send confirmation to club owner.
            reservationRepo.save(reservation);
            reservationResponse = new ReservationResponse().toBuilder()
                    .reserved(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while adding new reservation");
            throw new CommonException("Something went wrong");
        }
        return reservationResponse;
    }

    @Override
    public PaginatedResponse<GetReservationsResponse> getReservations(Authorisation authorisation,Integer page) throws Exception {
        long totalCount;
        long offset = page * 5;
        List<Map<String, Object>> reservationList ;

        List<String> cancelledReservationStatus = PartyAppConstants.CANCELLED_RESERVATION_STATUS;
        reservationList = reservationRepo.findByUserIdAndBookedDateAfter(authorisation.getUserId(),new Date(),cancelledReservationStatus,offset,3);
        totalCount = reservationRepo.countReservationByBookedDateAfter(authorisation.getUserId(),new Date(),cancelledReservationStatus);
        if(ObjectUtils.isEmpty(reservationList)){
            throw new ContentNotFoundException("No Records to display");
        }
        if(totalCount == 0){
            return new PaginatedResponse<>(page,0,totalCount,5,new GetReservationsResponse(),GetReservationsResponse.class);
        }
        List<GetReservationsResponse.GetReservationsResponseModel> getReservationsResponseModelList = new ArrayList<>();
        reservationList.stream().forEach(e->{
            GetReservationsResponse.GetReservationsResponseModel getReservationsResponseModel = new GetReservationsResponse.GetReservationsResponseModel().toBuilder()
                    .id(Long.valueOf(e.get("id").toString()))
                    .clubName(e.get("club_name").toString())
                    .persons(Integer.valueOf(e.get("persons").toString()))
                    .bookedDate((Date) e.get("booked_date"))
                    .bookedTime((Time) e.get("booked_time"))
                    .status(e.get("status").toString())
                    .build();
            getReservationsResponseModelList.add(getReservationsResponseModel);
        });

        GetReservationsResponse getReservationResponse;
        getReservationResponse = new GetReservationsResponse().toBuilder()
                .reservationsResponseModelList(getReservationsResponseModelList)
                .build();
        return new PaginatedResponse<>(page,getReservationsResponseModelList.size(),totalCount,5, getReservationResponse,GetReservationsResponse.class);
    }

    @Override
    public CancelReservationResponse cancelReservation(CancelReservationRequest cancelReservationRequest, Authorisation authorisation) throws Exception {
        if(ObjectUtils.isEmpty(cancelReservationRequest.getReservationId())){
            throw new InCompleteDetailsException("Missing details");
        }
        Optional<Reservation> reservation = reservationRepo.findById(cancelReservationRequest.getReservationId());
        if(ObjectUtils.isEmpty(reservation)){
            throw new InValidRequestException("Bad Request");
        }
        if(!reservation.get().getUserId().equals(authorisation.getUserId())){
            throw new InValidRequestException("Invalid Request");
        }
        CancelReservationResponse cancelReservationResponse = new CancelReservationResponse();
        try {
            reservation.get().setStatus(ReservationStatus.CANCELLED_BY_USER.getValue());
            reservationRepo.save(reservation.get());
        }
        catch (Exception e){
            log.error("Error occurred while cancelling request = {}",e.toString());
            cancelReservationResponse.setSuccess(Boolean.FALSE);
            return  cancelReservationResponse;
        }
        cancelReservationResponse.setSuccess(Boolean.TRUE);
        return cancelReservationResponse;
    }
}
