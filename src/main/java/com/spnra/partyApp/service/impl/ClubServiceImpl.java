package com.spnra.partyApp.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spnra.partyApp.constants.PartyAppConstants;
import com.spnra.partyApp.dto.request.ClubCreationRequest;
import com.spnra.partyApp.dto.request.ClubDetailCreationRequest;
import com.spnra.partyApp.dto.request.ClubInfoUpdateRequest;
import com.spnra.partyApp.dto.response.ClubCreationResponse;
import com.spnra.partyApp.dto.response.ClubInfoUpdateResponse;
import com.spnra.partyApp.dto.response.ClubInterestResponse;
import com.spnra.partyApp.entity.*;
import com.spnra.partyApp.enums.City;
import com.spnra.partyApp.enums.InfoType;
import com.spnra.partyApp.exceptions.*;
import com.spnra.partyApp.model.S3DocUploader;
import com.spnra.partyApp.repo.*;
import com.spnra.partyApp.service.ClubService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class ClubServiceImpl implements ClubService {

    @Autowired
    private ClubInterestRepo clubInterestRepo;

    @Autowired
    private ClubRepo clubRepo;

    @Autowired
    private DetailRepo detailRepo;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private S3DocUploader s3DocUploader;

    @Value(("${s3.key.extra}"))
    private String s3KeyExtra;

    @Override
    public ClubInfoUpdateResponse updateClubImages(ClubInfoUpdateRequest clubInfoUpdateRequest) throws Exception {
        ClubInfoUpdateResponse clubInfoUpdateResponse;
        log.info("Received Club Info update request: {}", clubInfoUpdateRequest.toString());
        if(ObjectUtils.isEmpty(clubInfoUpdateRequest)){
            log.error("Empty request in club update info");
            throw new InValidRequestException("Invalid request");
        }
        if(!clubInfoUpdateRequest.getInfoType().equals(InfoType.CLUB.getValue())&&!clubInfoUpdateRequest.getInfoType().equals(InfoType.MENU.getValue())){
            log.error("Invalid request in club update info");
            throw new InValidRequestException("Invalid request");
        }
        Optional<Detail> clubInfo = detailRepo.findByClubId(clubInfoUpdateRequest.getClubId());
        if(ObjectUtils.isEmpty(clubInfo.get())){
            log.error("Empty request in club update info");
            throw new InValidRequestException("Invalid request");
        }
        HashMap<String,String> info= new HashMap<String,String>();
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic1())){
            info.put("pic1",clubInfoUpdateRequest.getPic1());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic2())){
            info.put("pic2",clubInfoUpdateRequest.getPic2());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic3())){
            info.put("pic3",clubInfoUpdateRequest.getPic3());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic4())){
            info.put("pic4",clubInfoUpdateRequest.getPic4());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic5())){
            info.put("pic5",clubInfoUpdateRequest.getPic5());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic6())){
            info.put("pic6",clubInfoUpdateRequest.getPic6());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic7())){
            info.put("pic7",clubInfoUpdateRequest.getPic7());
        }
        if(!ObjectUtils.isEmpty(clubInfoUpdateRequest.getPic8())){
            info.put("pic8",clubInfoUpdateRequest.getPic8());
        }
        try {
            if (clubInfoUpdateRequest.getInfoType().equals(InfoType.CLUB.getValue())) {
                clubInfo.get().setPhotos(objectMapper.writeValueAsString(info));
            } else {
                clubInfo.get().setMenu(objectMapper.writeValueAsString(info));
            }
            detailRepo.save(clubInfo.get());
            clubInfoUpdateResponse = new ClubInfoUpdateResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Error occurred while updating club info with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        return clubInfoUpdateResponse;
    }

    @Override
    public ClubInterestResponse clubInterest(Long clubId, Authorisation authorisation) throws Exception {
        if(ObjectUtils.isEmpty(clubId)){
            throw new InValidRequestException("Bad Request");
        }
        ClubInterestResponse clubInterestResponse ;
        try{
            Optional<Club> club = clubRepo.findById(clubId);
            if(ObjectUtils.isEmpty(club)){
                throw new InValidRequestException("Invalid Request");
            }
            Optional<ClubInterest> ifExist = clubInterestRepo.findAllByClubIdAndUserId(clubId,authorisation.getUserId());
            if(ObjectUtils.isEmpty(ifExist)){
                ClubInterest clubInterest = new ClubInterest().toBuilder()
                        .clubId(club.get().getId())
                        .userId(authorisation.getUserId())
                        .cityId(club.get().getCityId())
                        .build();
                clubInterestRepo.save(clubInterest);
            }
            clubInterestResponse = new ClubInterestResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while creating event interest");
            clubInterestResponse = new ClubInterestResponse().toBuilder()
                    .success(Boolean.FALSE)
                    .build();
        }
        return clubInterestResponse;
    }

    @Override
    public ClubInterestResponse removeClubInterest(Long clubId, Authorisation authorisation) throws Exception {
        if(ObjectUtils.isEmpty(clubId)){
            throw new NotLoggedInException("Bad Request");
        }
        ClubInterestResponse clubInterestResponse ;
        if(ObjectUtils.isEmpty(authorisation)){
            throw new InValidRequestException("Bad Request");
        }
        try{
            Optional<Club> club = clubRepo.findById(clubId);
            if(ObjectUtils.isEmpty(club)){
                throw new InValidRequestException("Invalid Request");
            }
            Optional<ClubInterest> ifExist = clubInterestRepo.findAllByClubIdAndUserId(clubId,authorisation.getUserId());
            if(!ObjectUtils.isEmpty(ifExist)){
                clubInterestRepo.deleteById(ifExist.get().getId());
            }
            clubInterestResponse = new ClubInterestResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while creating event interest");
            clubInterestResponse = new ClubInterestResponse().toBuilder()
                    .success(Boolean.FALSE)
                    .build();
        }
        return clubInterestResponse;
    }

    @Override
    public ClubCreationResponse createClub(ClubCreationRequest clubCreationRequest) throws Exception {
        if(ObjectUtils.isEmpty(clubCreationRequest)){
            throw new InValidRequestException("Bad Request");
        }
        ClubCreationResponse clubCreationResponse;
        try {
            Club club = new Club().toBuilder()
                    .name(clubCreationRequest.getName())
                    .contactPerson(clubCreationRequest.getContactPerson()==null?"":clubCreationRequest.getContactPerson())
                    .mobileNumber(clubCreationRequest.getMobileNumber())
                    .phoneNumber(clubCreationRequest.getPhoneNumber())
                    .address(clubCreationRequest.getAddress())
                    .area(clubCreationRequest.getArea())
                    .cityId(City.valueOf(clubCreationRequest.getCity().toUpperCase(Locale.ROOT)).getType())
                    .addressLink(clubCreationRequest.getAddressLink())
                    .image(clubCreationRequest.getImage())
                    .latitude(clubCreationRequest.getLatitude())
                    .longitude(clubCreationRequest.getLongitude())
                    .discount(clubCreationRequest.getDiscount()==null?"":clubCreationRequest.getDiscount())
                    .priority("5")
                    .status(Boolean.TRUE)
                    .timings(objectMapper.writeValueAsString(clubCreationRequest.getTiming()))
                    .email(clubCreationRequest.getEmail()==null?"":clubCreationRequest.getEmail())
                    .build();
            clubRepo.save(club);
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while adding new club with exception = {}",e.toString());
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.FALSE)
                    .build();
        }
        return  clubCreationResponse;
    }

    @Override
    public HashMap<String, String> uploadClubImages(Map<String, MultipartFile> fileMap) throws Exception {
        HashMap<String, String> hm = new HashMap<>();
        for (Map.Entry m : fileMap.entrySet()) {
            File doc = s3DocUploader.convertMultiPartToFile((MultipartFile) m.getValue());
            String file_name ;
            try{
                file_name = (String) m.getKey();
                String extension = ((MultipartFile) m.getValue()).getContentType();
                String fileExtension= FilenameUtils.getExtension(((MultipartFile) m.getValue()).getOriginalFilename());
                if(!PartyAppConstants.ALLOWED_FILE_TYPE.contains(extension)){
                    throw new InCorrectFileTypeException("Incorrect File type");
                }
                if(!PartyAppConstants.ALLOWED_FILE_TYPE.contains(fileExtension.toLowerCase(Locale.ROOT))){
                    throw new InCorrectFileTypeException("Incorrect File type");
                }
            }
            catch (Exception e){
                throw new CommonException("Some Error Occurred");
            }
            try {
                OutputStream oos = new FileOutputStream("/tmp/" + file_name);
                byte[] buf = new byte[8192];
                InputStream is = new FileInputStream(doc);

                int c = 0;
                while ((c = is.read(buf, 0, buf.length)) > 0) {
                    oos.write(buf, 0, c);
                    oos.flush();
                }
                s3DocUploader.uploadFileToS3("/tmp/" + file_name,s3KeyExtra + "/" + file_name);
                hm.put(m.getKey().toString(), PartyAppConstants.S3_EXTRA_DOC_URL  +  file_name);
                log.info("Successful execution and uploading of user Documents: {}",
                        new File(file_name));
            } catch (Exception e) {
                log.info("Error in uploading user Documents with Exception {}",e.toString());
                throw new Exception("Some Error Occurred");
            }
            doc.delete();
        }
        return hm;
    }

    @Override
    public ClubCreationResponse createClubDetail(ClubDetailCreationRequest clubDetailCreationRequest) throws Exception {
        if(ObjectUtils.isEmpty(clubDetailCreationRequest)){
            throw new InValidRequestException("Bad Request");
        }
        Optional<Club> club = clubRepo.findByIdAndStatus(Long.valueOf(clubDetailCreationRequest.getClubId()),true);
        if(ObjectUtils.isEmpty(club)){
            throw new InValidRequestException("Bad Request");
        }
        ClubCreationResponse clubCreationResponse;
        try {
            Detail detail = new Detail().toBuilder()
                    .clubId(club.get().getId())
                    .description(clubDetailCreationRequest.getDesc())
                    .beerRate(clubDetailCreationRequest.getBeerRate())
                    .beerText("Beer for 2 @")
                    .kitchenRate(clubDetailCreationRequest.getKitchenRate())
                    .kitchenText("Fooding for 2 @")
                    .daySpecial(objectMapper.writeValueAsString(clubDetailCreationRequest.getDaySpecial()))
                    .valet(clubDetailCreationRequest.getValet())
                    .timings(objectMapper.writeValueAsString(clubDetailCreationRequest.getTiming()))
                    .reservation(clubDetailCreationRequest.getReservation())
                    .build();

            detailRepo.save(detail);
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while adding new club detail with exception = {}",e.toString());
            clubCreationResponse = new ClubCreationResponse().toBuilder()
                    .success(Boolean.FALSE)
                    .build();
        }
        return  clubCreationResponse;
    }
}
