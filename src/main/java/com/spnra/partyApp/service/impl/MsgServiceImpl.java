package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.constants.PartyAppConstants;
import com.spnra.partyApp.exceptions.CommonException;
import com.spnra.partyApp.service.MsgService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Service
public class MsgServiceImpl implements MsgService {

    @Value(("${msg.link}"))
    private String msgLink;

    @Override
    public Boolean sendSms(String phone, String otp) throws Exception {
        String url = null;
        try {
            url = buildCreateConversionUrl(phone,otp);
        }
        catch(Exception e){
            log.error("Error while creating Sms creation url for phone_no = {} with exception = {}",phone,e.toString());
        }
        try {
            URL urlToHit = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlToHit.openConnection();
            connection.setRequestMethod("GET");
            connection.getInputStream();
            log.info("Success while hitting Sms Create API for URL = {}", url);
            return true;
        }
        catch (Exception e) {
            log.error("Error while hitting Sms Create API for URL = {}, with Exception = {}", url, e.toString());
            throw new CommonException("Some Error Occurred");
        }
    }

    private String buildCreateConversionUrl(String phone,String otp) {

        String beforeSms = PartyAppConstants.beforeMsg;
        String afterSms = PartyAppConstants.afterMsg;
        String firstSms = PartyAppConstants.firstMsg;
        String url = msgLink;
        url = url + firstSms + phone + beforeSms + otp + afterSms;
        return url;
    }
}
