package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.dto.Admin.AdminUserRequest;
import com.spnra.partyApp.dto.Admin.AdminUserResponse;
import com.spnra.partyApp.dto.request.OtpVerificationRequest;
import com.spnra.partyApp.dto.request.UserProfileUpdateRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.entity.Club;
import com.spnra.partyApp.entity.ClubAdmin;
import com.spnra.partyApp.enums.SignUpState;
import com.spnra.partyApp.enums.UserType;
import com.spnra.partyApp.exceptions.*;
import com.spnra.partyApp.model.InternalClubAdminResponse;
import com.spnra.partyApp.repo.AuthorisationRepo;
import com.spnra.partyApp.repo.ClubAdminRepo;
import com.spnra.partyApp.repo.ClubRepo;
import com.spnra.partyApp.service.AdminUserService;
import com.spnra.partyApp.service.TokenService;
import com.spnra.partyApp.utils.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

@Slf4j
@Service
public class AdminUserImpl implements AdminUserService {

    @Autowired
    private ClubAdminRepo clubAdminRepo;

    @Autowired
    private ClubRepo clubRepo;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AuthorisationRepo authorisationRepo;

    @Override
    public AdminUserResponse createClubAdmin(AdminUserRequest adminUserRequest) throws Exception {
        log.info("Received User Create Request: {}", adminUserRequest);
        if (ObjectUtils.isEmpty(adminUserRequest)) {
            return null;
        }
        if (ObjectUtils.isEmpty(adminUserRequest.getName()) || ObjectUtils.isEmpty(adminUserRequest.getPhone())||ObjectUtils.isEmpty(adminUserRequest.getClubId())) {
            log.error("Incomplete details while trying to signup");
            throw new InCompleteDetailsException("Missing details");
        }

        Boolean check = AppUtils.phoneNumberValidate(adminUserRequest.getPhone());
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }

        Optional<ClubAdmin> clubAdmin = clubAdminRepo.findByPhoneNumber(adminUserRequest.getPhone());
        if (!ObjectUtils.isEmpty(clubAdmin)) {
            throw new UserAlreadyExistException("User already exist for this phone number");
        }

        Optional<Club> club = clubRepo.findById(adminUserRequest.getClubId());
        if (!ObjectUtils.isEmpty(clubAdmin)) {
            throw new InValidRequestException("Club doesn't exist");
        }

        AdminUserResponse adminUserResponse;
        try {
            ClubAdmin userCreate = new ClubAdmin()
                    .toBuilder()
                    .name(adminUserRequest.getName())
                    .phoneNumber(adminUserRequest.getPhone())
                    .signUpStatus(SignUpState.OTP_SENT.getValue())
                    .emailVerified(Boolean.FALSE)
                    .status(Boolean.TRUE)
                    .clubId(adminUserRequest.getClubId())
                    .build();
            clubAdminRepo.save(userCreate);
        } catch (Exception e) {
            log.error("Error occurred while creating new user : {} with exception :{}", adminUserRequest.getPhone(), e.toString());
            throw new CommonException("Some Error Occurred");
        }
        log.info("Successfully created user with phone number: {}", adminUserRequest.getPhone());
        InternalClubAdminResponse signUpUser = getInternalClubAdmin(null,adminUserRequest.getPhone());
        try {
            signInClubAdmin(signUpUser.getPhone());
        } catch (Exception e) {
            log.error("Some error occurred while signing in with this number: {} with exception:{}", adminUserRequest.getPhone(), e.toString());
            throw new CommonException("Some Error Occurred");
        }
        try {
            adminUserResponse = new AdminUserResponse().toBuilder()
                    .name(signUpUser.getName())
                    .phone(signUpUser.getPhone())
                    .email(signUpUser.getEmail())
                    .build();
        } catch (Exception e) {
            log.error("Some error occurred while fetching details for this number: {} with exception:{}", adminUserRequest.getPhone(), e.toString());
            throw new CommonException("Some Error Occurred");
        }
        return adminUserResponse;
    }

    @Override
    public AdminUserResponse getClubAdmin(Long userId, String phone) throws Exception {
        if (ObjectUtils.isEmpty(userId) && ObjectUtils.isEmpty(phone)) {
            log.error("Invalid Request to get user");
            throw new InValidRequestException("Invalid Request to get exception");
        }
        AdminUserResponse userResponse;
        Optional<ClubAdmin> clubAdmin;
        if (!ObjectUtils.isEmpty(userId)) {
            clubAdmin = clubAdminRepo.findById(userId);
        } else {
            clubAdmin = clubAdminRepo.findByPhoneNumber(phone);
        }
        if (ObjectUtils.isEmpty(clubAdmin)) {
            throw new UserNotFoundException("Club Admin Not Found");
        }
        userResponse = new AdminUserResponse().toBuilder()
                .name(clubAdmin.get().getName())
                .phone(clubAdmin.get().getPhoneNumber())
                .email(clubAdmin.get().getEmail())
                .build();
        return userResponse;
    }

    @Override
    public InternalClubAdminResponse getInternalClubAdmin(Long userId, String phone) throws Exception {
        if (ObjectUtils.isEmpty(userId) && ObjectUtils.isEmpty(phone)) {
            log.error("Invalid Request to get user");
            throw new InValidRequestException("Invalid Request to get exception");
        }
        InternalClubAdminResponse internalClubAdminResponse;
        Optional<ClubAdmin> clubAdmin;
        if (!ObjectUtils.isEmpty(userId)) {
            clubAdmin = clubAdminRepo.findById(userId);
        } else {
            clubAdmin = clubAdminRepo.findByPhoneNumber(phone);
        }
        if (ObjectUtils.isEmpty(clubAdmin)) {
            throw new UserNotFoundException("Club Admin Not Found");
        }
        internalClubAdminResponse = new InternalClubAdminResponse().toBuilder()
                .id(clubAdmin.get().getId())
                .name(clubAdmin.get().getName())
                .phone(clubAdmin.get().getPhoneNumber())
                .email(clubAdmin.get().getEmail())
                .build();
        return internalClubAdminResponse;
    }

    @Override
    public SignInResponse signInClubAdmin(String phone) throws Exception {
        SignInResponse signInResponse;
        log.info("Received User Sign In Request: {}", phone);
        if (ObjectUtils.isEmpty(phone)) {
            throw new InValidRequestException("Missing details");
        }
        Boolean check = AppUtils.phoneNumberValidate(phone);
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }
        InternalClubAdminResponse user;
        try {
            user = getInternalClubAdmin(null,phone);
        }
        catch (UserNotFoundException e) {
            log.error("No club admin user Found with phone number", phone);
            throw new UserNotFoundException("User Not Found");
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching club admin details = {}", e.toString(), phone);
            throw new CommonException("Some Error Occurred");
        }
        tokenService.sendOtp(user.getId(),user.getPhone(),10, UserType.ADMIN.getValue());
        signInResponse = new SignInResponse().toBuilder()
                .otpSent(Boolean.TRUE.booleanValue())
                .build();
        return signInResponse;
    }

    @Override
    public SignOutResponse signOutClubAdmin(Long id) throws Exception {
        SignOutResponse signOutResponse;
        if (ObjectUtils.isEmpty(id)) {
            throw new InValidRequestException("Missing id");
        }
        try {
            authorisationRepo.deleteById(id);
            signOutResponse = new SignOutResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        } catch (Exception e) {
            log.error("some error occurred while signing out with exception = {}", e.toString());
            throw new CommonException("Some error occurred");
        }
        return signOutResponse;
    }

    @Override
    public OtpVerificationResponse otpVerifyClubAdmin(OtpVerificationRequest otpVerificationRequest) throws Exception {
        OtpVerificationResponse otpVerificationResponse;
        log.info("Received User Otp Verification Request: {}", otpVerificationRequest.getPhone());
        if (ObjectUtils.isEmpty(otpVerificationRequest.getPhone())||ObjectUtils.isEmpty(otpVerificationRequest.getOtp())) {
            throw new InValidRequestException("Missing details");
        }

        Boolean check = AppUtils.phoneNumberValidate(otpVerificationRequest.getPhone());
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }
        InternalClubAdminResponse userResponse;
        try {
            userResponse = getInternalClubAdmin(null,otpVerificationRequest.getPhone());
        }
        catch (UserNotFoundException e) {
            log.error("No club admin Found with phone number", otpVerificationRequest.getPhone());
            throw new UserNotFoundException("User Not Found");
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching user details = {}", e.toString(), otpVerificationRequest.getPhone());
            throw new CommonException("Some Error Occurred");
        }
        String eId = tokenService.verifyOtp(userResponse.getId(),otpVerificationRequest.getOtp(),UserType.ADMIN.getValue());
        verifyUser(userResponse.getId());
        otpVerificationResponse = new OtpVerificationResponse().toBuilder()
                .otpVerified(Boolean.TRUE)
                .authorisation(eId)
                .build();
        return otpVerificationResponse;
    }

    @Override
    public SignInResponse otpResendClubAdmin(String phone) throws Exception {
        SignInResponse signInResponse = null;
        if (ObjectUtils.isEmpty(phone)) {
            return null;
        }
        Boolean check = AppUtils.phoneNumberValidate(phone);
        if (!check) {
            throw new InValidPhoneNumberException("Phone number not valid");
        }
        log.info("Received User Resend Otp Request: {}", phone);
        InternalClubAdminResponse userResponse;
        try {
            userResponse = getInternalClubAdmin(null,phone);
        }
        catch (UserNotFoundException e) {
            log.error("No club admin Found with phone number",phone);
            throw new UserNotFoundException("User Not Found");
        }
        catch (Exception e) {
            log.error("Some Exception occurred while fetching club admin details = {}", e.toString(),phone);
            throw new CommonException("Some Error Occurred");
        }
        tokenService.sendOtp(userResponse.getId(),userResponse.getPhone(),30,UserType.ADMIN.getValue());
        signInResponse = new SignInResponse().toBuilder()
                .otpSent(Boolean.TRUE.booleanValue())
                .build();
        return signInResponse;
    }

    @Override
    public UserProfileUpdateResponse updateClubDetails(UserProfileUpdateRequest userProfileUpdateRequest, Authorisation authorisation) throws Exception {
        return null;
    }

    @Async
    public void verifyUser(Long userId) {
        try {
            Optional<ClubAdmin> user = clubAdminRepo.findById(userId);
            user.get().setSignUpStatus(SignUpState.OTP_VERIFIED.getValue());
            clubAdminRepo.save(user.get());
        } catch (Exception e) {
            log.error("Error occurred while updating user sign up status with expception = {}", e.toString());
        }
    }
}
