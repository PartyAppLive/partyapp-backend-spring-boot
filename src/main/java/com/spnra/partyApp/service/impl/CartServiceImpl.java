package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.dto.request.OfferAddRequest;
import com.spnra.partyApp.dto.request.TicketOrderRequest;
import com.spnra.partyApp.dto.response.ClubCreationResponse;
import com.spnra.partyApp.dto.response.UserCartResponse;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.entity.Cart;
import com.spnra.partyApp.entity.CartDetails;
import com.spnra.partyApp.entity.Offer;
import com.spnra.partyApp.exceptions.CommonException;
import com.spnra.partyApp.exceptions.DifferentClubOfferException;
import com.spnra.partyApp.exceptions.InValidRequestException;
import com.spnra.partyApp.exceptions.OutOfStockException;
import com.spnra.partyApp.repo.CartDetailsRepo;
import com.spnra.partyApp.repo.CartRepo;
import com.spnra.partyApp.repo.OfferRepo;
import com.spnra.partyApp.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;

@Slf4j
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartDetailsRepo cartDetailsRepo;

    @Autowired
    private CartRepo cartRepo;

    @Autowired
    private OfferRepo offerRepo;

    @Override
    public UserCartResponse getCart(Long userId) throws Exception {
        Optional<Cart> cart;
        Optional<List<CartDetails>> cartDetailsList = null;
        try {
            cart = cartRepo.findByUserIdAndStatus(userId, Boolean.TRUE);
            if (ObjectUtils.isEmpty(cart)) {
                throw new InValidRequestException("Some Error occurred");
            }
        }
        catch (Exception e){
            log.error("Error occurred while fetching offer with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        return this.inventoryCheck(cart.get().getId());
    }

    @Override
    public UserCartResponse addProduct(OfferAddRequest offerAddRequest,Authorisation authorisation) throws Exception {
        if(ObjectUtils.isEmpty(offerAddRequest)){
            throw new InValidRequestException("Some error occurred");
        }
        Optional<Offer> offer;
        Optional<Cart> cart;
        Optional<List<CartDetails>> cartDetailsList = null;
        try{
            offer = offerRepo.findById(offerAddRequest.getOfferId());
            if(!offer.isPresent()){
                throw new InValidRequestException("Some Error occurred");
            }
            if(offer.get().getInventory()-offer.get().getSoldUnit()<1){
                throw new OutOfStockException("Product is Out Of Stock");
            }
            if(!offer.get().getStatus()){
                throw new InValidRequestException("Invalid Request");
            }
        }
        catch (OutOfStockException e){
            throw new OutOfStockException("Product is Out Of Stock");
        }
        catch (Exception e){
            log.error("Error occurred while fetching offer with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        try{
            cart = cartRepo.findByUserIdAndStatus(authorisation.getUserId(),Boolean.TRUE);
            if(ObjectUtils.isEmpty(cart)){
                Cart newCart = new Cart().toBuilder()
                        .status(Boolean.TRUE)
                        .userId(authorisation.getUserId())
                        .build();
                cartRepo.save(newCart);
                cart = cartRepo.findByUserIdAndStatus(authorisation.getUserId(),Boolean.TRUE);
                CartDetails cartDetails = new CartDetails().toBuilder()
                        .cartId(cart.get().getId())
                        .userId(authorisation.getUserId())
                        .offerId(offer.get().getId())
                        .offerType(offer.get().getOfferType())
                        .quantity(1)
                        .build();
                cartDetailsRepo.save(cartDetails);
            }
            else{
                cartDetailsList = cartDetailsRepo.findByCartId(cart.get().getId());
                Boolean cartCheck = cartItemValidate(cartDetailsList.get(),offer.get());
                if(!cartCheck) {
                    List<Long> idToDelete =  new ArrayList<>();
                    for(CartDetails cartDetails: cartDetailsList.get()){
                        idToDelete.add(cartDetails.getId());
                    }
                    cartDetailsRepo.deleteAllByIdIn(idToDelete);
                    CartDetails cartDetails = new CartDetails().toBuilder()
                            .cartId(cart.get().getId())
                            .userId(authorisation.getUserId())
                            .offerId(offer.get().getId())
                            .offerType(offer.get().getOfferType())
                            .quantity(1)
                            .build();
                    cartDetailsRepo.save(cartDetails);
                }
                else{
                    int temp = 0 ;
                    for(CartDetails cartDetails: cartDetailsList.get()){
                        if(cartDetails.getOfferId().equals(offer.get().getId())){
                            cartDetails.setQuantity(cartDetails.getQuantity()+1);
                            cartDetailsRepo.save(cartDetails);
                            temp = 1;
                            break;
                        }
                    }
                    if(temp==0) {
                        CartDetails cartDetails = new CartDetails().toBuilder()
                                .cartId(cart.get().getId())
                                .userId(authorisation.getUserId())
                                .offerId(offer.get().getId())
                                .offerType(offer.get().getOfferType())
                                .quantity(1)
                                .build();
                        cartDetailsRepo.save(cartDetails);
                    }
                }
            }
            return this.inventoryCheck(cart.get().getId());
        }
        catch (Exception e){
            log.error("Error occurred while fetching user's current cart with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }

    }

    @Override
    public UserCartResponse removeProduct(OfferAddRequest offerAddRequest,Authorisation authorisation) throws Exception{
        if(ObjectUtils.isEmpty(offerAddRequest)){
            throw new InValidRequestException("Some error occurred");
        }
        Optional<Offer> offer;
        Optional<Cart> cart;
        Optional<List<CartDetails>> cartDetailsList = null;
        try{
            offer = offerRepo.findById(offerAddRequest.getOfferId());
            if(!offer.isPresent()){
                throw new InValidRequestException("Some Error occurred");
            }

        }
        catch (Exception e){
            log.error("Error occurred while fetching offer with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        try {
            cart = cartRepo.findByUserIdAndStatus(authorisation.getUserId(), Boolean.TRUE);
            if (ObjectUtils.isEmpty(cart)) {
                throw new InValidRequestException("Some Error occurred");
            }
        }
        catch (Exception e){
            log.error("Error occurred while fetching offer with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        try {
            cartDetailsList = cartDetailsRepo.findByCartId(cart.get().getId());
            if (ObjectUtils.isEmpty(cartDetailsList)) {
                throw new InValidRequestException("Some Error occurred");
            }
        }
        catch (Exception e){
            log.error("Error occurred while fetching offer with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        int temp = 0;
        for(CartDetails cartDetails: cartDetailsList.get()){
            if(cartDetails.getOfferId().equals(offer.get().getId())){
                if(cartDetails.getQuantity()-1==0){
                    cartDetailsRepo.deleteById(cartDetails.getId());
                }
                else {
                    cartDetails.setQuantity(cartDetails.getQuantity() - 1);
                    cartDetailsRepo.save(cartDetails);
                }
                temp=1;
                break;
            }
        }
        if(temp==0){
            throw new CommonException("Some error occurred");
        }
        return inventoryCheck(cart.get().getId());
    }


    @Override
    public Object placeOrder(Long userId){
        //cartValidate
        //check if of same club
        //block inventory.
        //place an order in pending state.
        //razorpay

        return  null;
    }

    @Override
    public Object placeTicketOrder(Long userId, TicketOrderRequest ticketOrderRequest) {
        //cart validate
        //block inventory
        //place an order in pending state.
        //razorpay
        return  null;
    }

    private Boolean cartItemValidate(List<CartDetails> cartDetailsList,Offer offer) throws CommonException {
        if(ObjectUtils.isEmpty(cartDetailsList)||ObjectUtils.isEmpty(offer)){
            log.error("empty objects for cart item validate = {} , {}",cartDetailsList,offer);
            throw new CommonException("Some error occurred");
        }
        HashSet<Long> offerIds = new HashSet<>();
        for(CartDetails cartDetails: cartDetailsList){
            offerIds.add(cartDetails.getOfferId());
        }
        List<Offer> offerList = offerRepo.findAllByIdIn(offerIds);
        HashSet<Long> clubIdList = new HashSet<>();
        clubIdList.add(offer.getClubId());
        for(Offer currentOffer: offerList){
            clubIdList.add(currentOffer.getClubId());
        }
        if(clubIdList.size()>1){
            return false;
        }
        return true;
    }

    private UserCartResponse inventoryCheck(Long cartId) throws Exception{
        Optional<List<CartDetails>> cartDetailsList= cartDetailsRepo.findByCartId(cartId);
        if(ObjectUtils.isEmpty(cartDetailsList)){
            throw new CommonException("Some error occurred");
        }
        HashSet<Long> offerIds =  new HashSet<>();
        for(CartDetails cartDetails: cartDetailsList.get()){
            offerIds.add(cartDetails.getOfferId());
        }
        UserCartResponse userCartResponse;
        List<UserCartResponse.CartItem> cartItemList = new ArrayList<>();
        List<Offer> offerList = offerRepo.findAllByIdIn(offerIds);
        if(ObjectUtils.isEmpty(offerList)){
            throw new CommonException("Some error occurred");
        }
        HashMap<Long,Offer> offerHashMap =  new HashMap<>();
        for(Offer offer: offerList){
            offerHashMap.put(offer.getId(),offer);
        }
        int finalAmount = 0;
        int finalMrp = 0 ;
        for(CartDetails cartDetails: cartDetailsList.get()){
            Offer currentOffer = offerHashMap.get(cartDetails.getOfferId());
            finalAmount = finalAmount + cartDetails.getQuantity()*currentOffer.getDiscountedPrice();
            finalMrp = finalMrp + cartDetails.getQuantity()*currentOffer.getOriginalPrice();
            UserCartResponse.CartItem cartItem = new UserCartResponse.CartItem().toBuilder()
                    .offerId(currentOffer.getId())
                    .desc(currentOffer.getDesc())
                    .image(currentOffer.getImage())
                    .currentQuantity(cartDetails.getQuantity())
                    .originalPrice(cartDetails.getQuantity()*currentOffer.getOriginalPrice())
                    .discountedPrice(cartDetails.getQuantity()*currentOffer.getDiscountedPrice())
                    .totalPrice(cartDetails.getQuantity()*currentOffer.getDiscountedPrice())
                    .build();
            if((cartDetails.getQuantity()>currentOffer.getInventory()-currentOffer.getSoldUnit())||!currentOffer.getStatus()){
                cartItem.setIsExpired(Boolean.TRUE);
                cartItem.setIsExpiredReason("Out Of Stock");
            }
            cartItemList.add(cartItem);
        }
        userCartResponse = new UserCartResponse().toBuilder()
                .finalAmount(finalAmount)
                .cartItemList(cartItemList)
                .finalMrp(finalMrp)
                .userId(cartDetailsList.get().get(0).getUserId())
                .totalDiscount(finalMrp- finalAmount)
                .build();
        return userCartResponse;
    }
}
