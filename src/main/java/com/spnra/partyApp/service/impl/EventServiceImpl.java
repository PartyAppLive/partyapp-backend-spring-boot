package com.spnra.partyApp.service.impl;

import com.spnra.partyApp.dto.request.EventInterestRequest;
import com.spnra.partyApp.dto.request.EventRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.*;
import com.spnra.partyApp.enums.City;
import com.spnra.partyApp.exceptions.*;
import com.spnra.partyApp.repo.*;
import com.spnra.partyApp.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.*;

@Slf4j
@Service
public class EventServiceImpl  implements EventService {

    @Autowired
    private ClubRepo clubRepo;

    @Autowired
    private EventRepo eventRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private InterestRepo interestRepo;

    @Override
    public EventCreationResponse createEvent(EventRequest eventRequest) throws Exception {
        Optional<Club> club = clubRepo.findByIdAndStatus(eventRequest.getClubId(),true);
        if(ObjectUtils.isEmpty(club)){
            throw new InValidRequestException("Bad Request");
        }
        EventCreationResponse eventCreationResponse;
        try {
            Event event = new Event().toBuilder()
                    .eventDate(eventRequest.getEventDate())
                    .clubId(eventRequest.getClubId())
                    .ticketInfo(eventRequest.getTicketInfo())
                    .organiser(eventRequest.getOrganiser())
                    .venue(eventRequest.getVenue())
                    .address(eventRequest.getAddress())
                    .organiserNumber(eventRequest.getOrganiserNumber())
                    .remarks(eventRequest.getRemarks())
                    .eventTime(eventRequest.getEventTime())
                    .city_id(City.valueOf(eventRequest.getCity().toUpperCase(Locale.ROOT)).getType())
                    .description(eventRequest.getDescription())
                    .eventName(eventRequest.getEventName())
                    .interest(0)
                    .image(eventRequest.getImage())
                    .phoneNumber(eventRequest.getPhoneNumber())
                    .status(Boolean.TRUE)
                    .build();
            eventRepo.save(event);

            eventCreationResponse = new EventCreationResponse().toBuilder()
                    .eventCreation(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while adding new event");
            eventCreationResponse = new EventCreationResponse().toBuilder()
                    .eventCreation(Boolean.FALSE)
                    .build();
        }
        return  eventCreationResponse;
    }

    @Transactional
    @Override
    public EventInterestResponse eventInterest(EventInterestRequest eventInterestRequest,Authorisation authorisation) throws Exception {
        if(ObjectUtils.isEmpty(eventInterestRequest.getEventId())){
            throw new InValidRequestException("Missing details");
        }
        EventInterestResponse eventInterestResponse ;
        Optional<Event> event = eventRepo.findById(eventInterestRequest.getEventId());
        if (ObjectUtils.isEmpty(event)) {
            throw new InValidRequestException("Bad Request");
        }
        Optional<Interest> ifExist = interestRepo.findAllByEventIdAndUserId(eventInterestRequest.getEventId(),authorisation.getUserId());
        if(!ObjectUtils.isEmpty(ifExist)){
            throw new AlreadyExistException("event is already added to favourites");
        }
        try{
            event.get().setInterest(event.get().getInterest() + 1);
            eventRepo.save(event.get());
            Interest interest = new Interest().toBuilder()
                    .eventId(event.get().getId())
                    .userId(authorisation.getUserId())
                    .build();

            interestRepo.save(interest);
            eventInterestResponse = new EventInterestResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while creating event interest with exception : {}",e.toString());
            throw new CommonException("Something went wrong");
        }
        return eventInterestResponse;
    }

    @Transactional
    @Override
    public EventInterestResponse removeEventInterest(EventInterestRequest eventInterestRequest,Authorisation authorisation) throws Exception {
        if(ObjectUtils.isEmpty(eventInterestRequest.getEventId())){
            throw new InValidRequestException("Bad Request");
        }
        EventInterestResponse eventInterestResponse ;
        try{
            Optional<Event> event = eventRepo.findById(eventInterestRequest.getEventId());
            if(ObjectUtils.isEmpty(event)){
                throw new InValidRequestException("Invalid Request");
            }
            Optional<Interest> ifExist = interestRepo.findAllByEventIdAndUserId(eventInterestRequest.getEventId(),authorisation.getUserId());
            if(ObjectUtils.isEmpty(ifExist)){
                throw new InValidRequestException("event is not set to favourites");
            }
            interestRepo.deleteById(ifExist.get().getId());
            event.get().setInterest(event.get().getInterest()-1);
            eventRepo.save(event.get());

            eventInterestResponse = new EventInterestResponse().toBuilder()
                    .success(Boolean.TRUE)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while removing event interest");
            throw new CommonException("Something went wrong");
        }
        return eventInterestResponse;
    }

    @Override
    public PaginatedResponse<EventPeopleResponse> peopleInterest(Integer eventId,Authorisation authorisation,Integer page) throws Exception {
        EventPeopleResponse eventPeopleResponse;
        if(ObjectUtils.isEmpty(eventId)){
            throw new InValidRequestException("Bad Request");
        }
        long totalCount;
        long offset = page * 25;
        List<EventPeopleResponse.PeopleInterestModel> peopleInterestModelList = new ArrayList<>();
        List<Map<String, Object>> peopleInterestList = new ArrayList<>();
        try{
        peopleInterestList = interestRepo.fetchAllInterestedPeopleBasedOnEventId(eventId,offset,25);
        totalCount = interestRepo.countAllByEventId(eventId.longValue());
        }
        catch(Exception e){
            log.error("Error occurred while getting people interested in an event with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        if(ObjectUtils.isEmpty(peopleInterestList)){
            throw new ContentNotFoundException("No Records to display");
        }
        if(totalCount == 0){
            return new PaginatedResponse<>(page,0,totalCount,25,new EventPeopleResponse(),EventPeopleResponse.class);
        }
        HashSet<Long> idList = new HashSet<>();
        peopleInterestList.stream().forEach(e->{
            idList.add(Long.valueOf(e.get("user_id").toString()));
        });
        List<User> userList = userRepo.findByIdInAndStatus(idList,true);
        for(User x : userList) {
            EventPeopleResponse.PeopleInterestModel peopleInterestModel = new EventPeopleResponse.PeopleInterestModel().toBuilder()
                    .name(x.getName())
                    .image(x.getImage())
                    .build();
            peopleInterestModelList.add(peopleInterestModel);
        }
        eventPeopleResponse = new EventPeopleResponse().toBuilder()
                .eventPeopleInterest(peopleInterestModelList)
                .build();
        return new PaginatedResponse<>(page,peopleInterestModelList.size(),totalCount,25, eventPeopleResponse,EventPeopleResponse.class);
    }

    @Override
    public PaginatedResponse<GetEventResponse> getAllEvents(Authorisation authorisation,Integer page,Long cityId) throws Exception {
        long totalCount;
        long offset = page * 3;
        GetEventResponse getEventResponse ;
        List<GetEventResponse.EventResponseModel> eventResponseModelList = new ArrayList<>();
        List<Map<String, Object>> eventList ;
        try {
            eventList = eventRepo.fetchAllEventsBasedOnCity(cityId, offset, 3);
            totalCount = eventRepo.getAllEventsCountBasedOnCity(cityId);
        }
        catch(Exception e){
                log.error("Error occurred while getting all events with exception = {}",e.toString());
                throw new CommonException("Some error occurred");
        }
        if(totalCount == 0|| ObjectUtils.isEmpty(totalCount)){
            return new PaginatedResponse<>(page,0,0,3,new GetEventResponse(),GetEventResponse.class);
        }
        if(ObjectUtils.isEmpty(eventList)){
            throw new ContentNotFoundException("No Records to display");
        }

        HashSet<Long> clubIdList = new HashSet<>();
        eventList.stream().forEach(e->{
            clubIdList.add(Long.valueOf(e.get("club_id").toString()));
        });
        List<Club> clubList = clubRepo.findByIdInAndStatus(clubIdList,true);
        HashMap<Long,String> hm = new HashMap<>();
        for(Club x : clubList){
            hm.put(x.getId(),x.getName());
        }

        HashSet<Long> eventIdList = new HashSet<>();
        eventList.stream().forEach(e->{
            eventIdList.add(Long.valueOf(e.get("id").toString()));
        });
        List<Interest> interestList = interestRepo.findByUserIdAndEventIdIn(authorisation.getUserId(), eventIdList);
        HashSet<Long> interestListMap = new HashSet<>();
        for(Interest x : interestList){
            interestListMap.add(x.getEventId());
        }

        try {
            eventList.stream().forEach(e->{
                GetEventResponse.EventResponseModel eventResponseModel = new GetEventResponse.EventResponseModel().toBuilder()
                        .id(Long.valueOf(e.get("id").toString()))
                        .clubName(hm.get(Long.valueOf((e.get("club_id").toString())))==null?"":hm.get(Long.valueOf((e.get("club_id").toString()))))
                        .eventDate((Date) e.get("event_date"))
                        .likeEvent(interestListMap.contains(Long.valueOf(e.get("id").toString()))?Boolean.TRUE:Boolean.FALSE)
                        .description(e.get("description").toString())
                        .image(e.get("image").toString())
                        .venue(e.get("venue").toString())
                        .address(e.get("address").toString())
                        .interest(Integer.valueOf(e.get("interest").toString()))
                        .phoneNumber(e.get("phone_number").toString())
                        .build();
                eventResponseModelList.add(eventResponseModel);
            });
            getEventResponse = new GetEventResponse().toBuilder()
                    .eventResponseModelList(eventResponseModelList)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while fetching today's events with exception = {}",e.toString());
            throw new CommonException("Something went wrong");
        }
        return new PaginatedResponse<>(page,eventResponseModelList.size(),totalCount,3, getEventResponse,GetEventResponse.class);

    }

    @Override
    public PaginatedResponse<GetEventResponse> getTodayEvents(Authorisation authorisation,Integer page,Long cityId) throws Exception {
        long totalCount;
        long offset = page * 3;
        GetEventResponse getEventResponse ;
        List<GetEventResponse.EventResponseModel> eventResponseModelList = new ArrayList<>();
        List<Map<String, Object>> eventList;
        try {
            eventList = eventRepo.fetchAllTodayEventsBasedOnCity(cityId, offset, 3);
            totalCount = eventRepo.getAllTodayEventsCountBasedOnCity(cityId);
        }
        catch(Exception e){
            log.error("Error occurred while getting today's events with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        if(ObjectUtils.isEmpty(eventList)){
            throw new ContentNotFoundException("No Records to display");
        }
        if(totalCount == 0){
            return new PaginatedResponse<>(page,0,0,3,new GetEventResponse(),GetEventResponse.class);
        }
        HashSet<Long> idList = new HashSet<>();
        eventList.stream().forEach(e->{
            idList.add(Long.valueOf(e.get("club_id").toString()));
        });
        List<Club> clubList = clubRepo.findByIdInAndStatus(idList,true);
        HashMap<Long,String> hm = new HashMap<>();
        for(Club x : clubList){
            hm.put(x.getId(),x.getName());
        }
        try {
            eventList.stream().forEach(e->{
                GetEventResponse.EventResponseModel eventResponseModel = new GetEventResponse.EventResponseModel().toBuilder()
                        .id(Long.valueOf(e.get("id").toString()))
                        .clubName(hm.get(Long.valueOf((e.get("club_id").toString())))==null?"":hm.get(Long.valueOf((e.get("club_id").toString()))))
                        .eventDate((Date) e.get("event_date"))
                        .description(e.get("description").toString())
                        .image(e.get("image").toString())
                        .venue(e.get("venue").toString())
                        .address(e.get("address").toString())
                        .interest(Integer.valueOf(e.get("interest").toString()))
                        .phoneNumber(e.get("phone_number").toString())
                        .build();
                eventResponseModelList.add(eventResponseModel);
            });

            getEventResponse = new GetEventResponse().toBuilder()
                    .eventResponseModelList(eventResponseModelList)
                    .build();
        }
        catch (Exception e){
            log.error("Some error occurred while fetching today's events");
            throw new CommonException("Something went wrong");
        }
        return new PaginatedResponse<>(page,eventResponseModelList.size(),totalCount,3, getEventResponse,GetEventResponse.class);
    }

    @Override
    public GetEventSearchResponse searchEvent(String keyWord) throws Exception {
        if (ObjectUtils.isEmpty(keyWord)) {
            throw new InValidRequestException("Bad Request");
        }
        List<GetEventSearchResponse.SearchResponseModel> homePageResponseModelList = new ArrayList<>();
        List<Map<String, Object>> eventList ;
        try {
            eventList = eventRepo.searchEventBasedOnKeyword(keyWord);
        }
        catch(Exception e){
            log.error("Error occurred while getting required event with exception = {}",e.toString());
            throw new CommonException("Some error occurred");
        }
        if(ObjectUtils.isEmpty(eventList)){
            throw new ContentNotFoundException("No Records to display");
        }
        eventList.stream().forEach(e->{
            GetEventSearchResponse.SearchResponseModel searchResponseModel = new GetEventSearchResponse.SearchResponseModel().toBuilder()
                    .id(Long.valueOf(e.get("id").toString()))
                    .name(e.get("event_name").toString())
                    .image(e.get("image").toString())
                    .build();
            homePageResponseModelList.add(searchResponseModel);
        });

        GetEventSearchResponse searchResponse;
        searchResponse = new GetEventSearchResponse().toBuilder()
                .searchEventResponseModelList(homePageResponseModelList)
                .build();
        return searchResponse;
    }
}
