package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.request.CitySelectRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.Authorisation;
import org.springframework.stereotype.Service;

public interface IndexService {

    PaginatedResponse<HomePageResponse> getAllClubs(Authorisation authorisation, Integer page,Long cityId) throws Exception;

    PaginatedResponse<HomePageResponse> getAllOfferClubs(Authorisation authorisation, Integer page,Long cityId) throws Exception;

    PdpPageResponse getClub(Authorisation authorisation, Long clubId) throws Exception;

    PaginatedResponse<HomePageResponse> getAllFavouriteClubs(Authorisation authorisation,Integer page,Long cityId) throws Exception;

    GetClubSearchResponse searchClub(Long cityId, String keyWord) throws Exception;

    GetLocationsResponse getLocations() throws Exception;

    ContactUsResponse getContactUs() throws  Exception;

}
