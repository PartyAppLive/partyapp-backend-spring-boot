package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.Admin.AdminUserRequest;
import com.spnra.partyApp.dto.Admin.AdminUserResponse;
import com.spnra.partyApp.dto.request.OtpVerificationRequest;
import com.spnra.partyApp.dto.request.UserProfileUpdateRequest;
import com.spnra.partyApp.dto.request.UserRequest;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.model.InternalClubAdminResponse;
import com.spnra.partyApp.model.InternalUserResponse;
import org.springframework.stereotype.Service;

public interface AdminUserService {

    AdminUserResponse createClubAdmin(AdminUserRequest adminUserRequest) throws Exception ;

    AdminUserResponse getClubAdmin(Long userId,String phone) throws Exception;

    InternalClubAdminResponse getInternalClubAdmin(Long userId, String phone) throws Exception;

    SignInResponse signInClubAdmin(String phone) throws Exception;

    SignOutResponse signOutClubAdmin(Long id) throws Exception;

    OtpVerificationResponse otpVerifyClubAdmin(OtpVerificationRequest otpVerificationRequest) throws Exception;

    SignInResponse otpResendClubAdmin(String phone) throws Exception;

    UserProfileUpdateResponse updateClubDetails(UserProfileUpdateRequest userProfileUpdateRequest, Authorisation authorisation) throws Exception;

}
