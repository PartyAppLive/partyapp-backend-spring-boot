package com.spnra.partyApp.service;

import com.spnra.partyApp.dto.response.UserResponse;
import com.spnra.partyApp.dto.request.*;
import com.spnra.partyApp.dto.response.*;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.model.InternalUserResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface UserService {

    UserResponse createUser(UserRequest userRequest) throws Exception ;

    UserResponse getUser(Long userId,String phone) throws Exception;

    InternalUserResponse getInternalUser(Long userId, String phone) throws Exception;

    SignInResponse signIn(String phone) throws Exception;

    SignOutResponse signOut(Long id) throws Exception;

    OtpVerificationResponse otpVerify(OtpVerificationRequest otpVerificationRequest) throws Exception;

    ClubInfoUpdateResponse selectCity(CitySelectRequest request,Authorisation authorisation) throws Exception;

    ImageUploadResponse dpUpload(Long userId, Map<String, MultipartFile> fileMap) throws Exception;

    SignInResponse otpResend(String phone) throws Exception;

    UserProfileUpdateResponse profileUpdate(UserProfileUpdateRequest userProfileUpdateRequest, Authorisation authorisation) throws Exception;

}
