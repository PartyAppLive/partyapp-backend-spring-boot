package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.dto.Paginable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Paginable(keyName="club")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class HomePageResponse {

    private List<HomePageResponseModel> homePageResponseModelList;

    @JsonProperty("error_code")
    private Integer errorCode = 0;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class  HomePageResponseModel {

        private Long id;

        private String name;

        private String image;

        private String discount;

        private String longitude;

        private String latitude;

        private String timing;

        private String area;

        private String directionLink;

    }
}
