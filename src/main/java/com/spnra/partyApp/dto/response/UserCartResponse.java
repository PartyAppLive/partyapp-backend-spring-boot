package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserCartResponse {

    private Long userId;

    private Integer finalAmount;

    private Integer finalMrp;

    private Integer totalDiscount;

    private List<CartItem> cartItemList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class CartItem {

        @JsonProperty("offer_id")
        private Long offerId;

        @JsonProperty("desc")
        private String desc;

        @JsonProperty("image")
        private String image;

        @JsonProperty("currentQuantity")
        private Integer currentQuantity;

        @JsonProperty("original_price")
        private Integer originalPrice;

        @JsonProperty("discounted_price")
        private Integer discountedPrice;

        @JsonProperty("is_expired")
        private Boolean isExpired;

        @JsonProperty("is_expired_reason")
        private String isExpiredReason;

        @JsonProperty("total_price")
        private Integer totalPrice;
    }

}
