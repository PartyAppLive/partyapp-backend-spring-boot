package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.dto.Paginable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Paginable(keyName="event")
@Builder(toBuilder = true)
public class GetEventResponse {

    private List<GetEventResponse.EventResponseModel> eventResponseModelList;

    @JsonProperty("error_code")
    private Integer errorCode = 0;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class  EventResponseModel {

        @JsonProperty("event_id")
        private Long id;

        @JsonProperty("club_name")
        private String clubName;

        @JsonProperty("image")
        private String image;

        @JsonProperty("phone_number")
        private String phoneNumber;

        @JsonProperty("interest")
        private Integer interest;

        @JsonProperty("like_event")
        private Boolean likeEvent;

        @JsonProperty("description")
        private String description;

        @JsonProperty("venue")
        private String venue;

        @JsonProperty("address")
        private String address;

        @JsonProperty("event_date")
        private Date eventDate;
    }
}
