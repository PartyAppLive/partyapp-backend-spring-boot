package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.dto.Paginable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Paginable(keyName="reservation")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetReservationsResponse {

    private List<GetReservationsResponse.GetReservationsResponseModel> reservationsResponseModelList;

    @JsonProperty("error_code")
    private Integer errorCode = 0;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class  GetReservationsResponseModel {

        @JsonProperty("id")
        private Long id;

        @JsonProperty("club_name")
        private String clubName;

        @JsonProperty("persons")
        private Integer persons;

        @JsonProperty("booked_date")
        private Date bookedDate;

        @JsonProperty("booked_time")
        private Time bookedTime;

        @JsonProperty("status")
        private String status;

    }


}
