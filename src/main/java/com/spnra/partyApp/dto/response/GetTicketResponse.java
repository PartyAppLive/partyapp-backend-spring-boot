package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.dto.Paginable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
public class GetTicketResponse {

    private List<GetTicketResponse.GetTicketResponseModel> ticketResponseModelList;

    @JsonProperty("error_code")
    private Integer errorCode = 0;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class GetTicketResponseModel {

        @JsonProperty("phase_id")
        private Long phaseId;

        @JsonProperty("event_id")
        private Long eventId;

        @JsonProperty("image")
        private String image;

        @JsonProperty("name")
        private String name;

        @JsonProperty("terms")
        private String terms;

        @JsonProperty("info")
        private String info;

        @JsonProperty("quantityLeft")
        private Integer quantityLeft;

        @JsonProperty("maxQuantity")
        private Integer maxQuantity;

        @JsonProperty("price")
        private Integer price;

        @JsonProperty("status")
        private Boolean status;
    }
}
