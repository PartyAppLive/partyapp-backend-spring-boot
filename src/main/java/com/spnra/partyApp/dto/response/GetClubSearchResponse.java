package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GetClubSearchResponse {

    private List<GetClubSearchResponse.SearchResponseModel> searchClubResponseModelList;

    @JsonProperty("error_code")
    private Integer errorCode = 0;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class  SearchResponseModel {

        private Long id;

        private String name;

        private String image;

    }
}
