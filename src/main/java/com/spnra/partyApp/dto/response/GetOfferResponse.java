package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.dto.Paginable;
import com.spnra.partyApp.dto.request.OfferCreationRequest;
import com.spnra.partyApp.model.DayOfWeekModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
public class GetOfferResponse {

    private List<GetOfferResponse.GetOfferResponseModel> offerResponseModelList;

    @JsonProperty("error_code")
    private Integer errorCode = 0;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class GetOfferResponseModel {

        @JsonProperty("offer_id")
        private Long offerId;

        @JsonProperty("club_id")
        private Long clubId;

        @JsonProperty("image")
        private String image;

        @JsonProperty("terms")
        private String terms;

        @JsonProperty("drinks_inclusions")
        private OfferCreationRequest.DrinkInclusionModel drinksInclusions;

        @JsonProperty("food_inclusions")
        private String foodInclusions;

        @JsonProperty("timings")
        private DayOfWeekModel timings;

        @JsonProperty("desc")
        private String desc;

        @JsonProperty("currentQuantity")
        private Integer currentQuantity;

        @JsonProperty("original_price")
        private Integer originalPrice;

        @JsonProperty("discounted_price")
        private Integer discountedPrice;

        @JsonProperty("discount_percentage")
        private Integer discountPercentage;
    }
}




