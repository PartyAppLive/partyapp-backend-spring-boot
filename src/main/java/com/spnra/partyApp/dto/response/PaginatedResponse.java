package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.dto.Paginable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PaginatedResponse<T> {

    private int page;

    private int noOfEntry;

    private long totalCount;

    private long rowsPerPage;

    /**
     * the key name shall be changed in the response according to the T
     */
    @JsonIgnore
    private T data;

    @JsonIgnore
    private Class<T> clazz;

    /**
     * This method is used in serialization for field data.
     * @return
     */
    @JsonAnyGetter
    public Map<String, Object> any() {
        Paginable p = (Paginable) clazz.getAnnotation(Paginable.class);
        return Collections.singletonMap(p.keyName() + "s", data);
    }
}