package com.spnra.partyApp.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PdpPageResponse {

    private Long clubId;

    private String description;

    private String mobileNumber;

    private String address;

    private String addressLink;

    private String beerText;

    private String kitchenText;

    private Integer beerRate;

    private Integer kitchenRate;

    @JsonRawValue
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "photos")
    private String photos;

    private String daySpecial;

    private String stags;

    private String timings;

    @JsonRawValue
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "menu")
    private String menu;

    private Boolean valet;

    private Boolean favourite;

    @JsonProperty("error_code")
    private Integer errorCode = 0;
}
