package com.spnra.partyApp.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.model.DayOfWeekModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ClubCreationRequest {

    @JsonProperty("name")
    private String name;

    @JsonProperty("contact_person")
    private String contactPerson;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("mobile_number")
    private String mobileNumber;

    @JsonProperty("address")
    private String address;

    @JsonProperty("area")
    private String area;

    @JsonProperty("image")
    private String image;

    @JsonProperty("email")
    private String email;

    @JsonProperty("timing")
    private DayOfWeekModel timing;

    @JsonProperty("address_link")
    private String addressLink;

    @JsonProperty("longitude")
    private Float longitude;

    @JsonProperty("latitude")
    private Float latitude;

    @JsonProperty("city")
    private String city;

    @JsonProperty("discount")
    private String discount;

}
