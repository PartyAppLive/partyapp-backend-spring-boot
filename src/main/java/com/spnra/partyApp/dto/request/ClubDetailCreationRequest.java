package com.spnra.partyApp.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.model.DayOfWeekModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ClubDetailCreationRequest {

    @JsonProperty("name")
    private String name;

    @JsonProperty("club_id")
    private Long clubId;

    @JsonProperty("desc")
    private String desc;

    @JsonProperty("kitchen_rate")
    private Integer kitchenRate;

    @JsonProperty("beer_rate")
    private Integer beerRate;

    @JsonProperty("valet")
    private Boolean valet;

    @JsonProperty("reservation")
    private Boolean reservation;

    @JsonProperty("day_special")
    private DayOfWeekModel daySpecial;

    @JsonProperty("timing")
    private DayOfWeekModel timing;
}
