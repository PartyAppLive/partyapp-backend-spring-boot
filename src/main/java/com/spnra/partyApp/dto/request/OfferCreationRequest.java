package com.spnra.partyApp.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spnra.partyApp.model.DayOfWeekModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OfferCreationRequest {

    @JsonProperty("club_id")
    private Long clubId;

    @JsonProperty("image")
    private String image;

    @JsonProperty("start_date")
    private Date startDate;

    @JsonProperty("end_date")
    private Date endDate;

    @JsonProperty("timing")
    private DayOfWeekModel dayOfWeekModel;

    @JsonProperty("desc")
    private String desc;

    @JsonProperty("offer_type")
    private String offerType;

    @JsonProperty("original_price")
    private Integer originalPrice;

    @JsonProperty("discounted_price")
    private Integer discountedPrice;

    @JsonProperty("discount_percentage")
    private Integer discountPercentage;

    @JsonProperty("inventory")
    private Integer inventory;

    @JsonProperty("terms")
    private String terms;

    @JsonProperty("drinks_inclusions")
    private DrinkInclusionModel drinkInclusionModel;

    @JsonProperty("food_inclusions")
    private List<String> foodInclusions;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class DrinkInclusionModel {

        @JsonProperty("beer")
        private List<String> beer;

        @JsonProperty("whiskey")
        private List<String> whiskey;

        @JsonProperty("gin")
        private List<String> gin;

        @JsonProperty("vodka")
        private List<String> vodka;

        @JsonProperty("rum")
        private List<String> rum;

        @JsonProperty("mocktail")
        private List<String> mocktail;

        @JsonProperty("mixers")
        private List<String> mixers;

        @JsonProperty("subject")
        private String subject;
    }
}
