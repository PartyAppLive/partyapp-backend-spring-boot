package com.spnra.partyApp.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
public class ClubInfoUpdateRequest {

    @JsonProperty("pic1")
    private String pic1;

    @JsonProperty("pic2")
    private String pic2;

    @JsonProperty("pic3")
    private String pic3;

    @JsonProperty("pic4")
    private String pic4;

    @JsonProperty("pic5")
    private String pic5;

    @JsonProperty("pic6")
    private String pic6;

    @JsonProperty("pic7")
    private String pic7;

    @JsonProperty("pic8")
    private String pic8;

    @JsonProperty("info_type")
    private String infoType;

    @JsonProperty("club_id")
    private Long clubId;
}
