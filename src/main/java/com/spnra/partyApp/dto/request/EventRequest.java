package com.spnra.partyApp.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
public class EventRequest {

    @JsonProperty("club_id")
    private Long clubId;

    @JsonProperty("event_name")
    private String eventName;

    @JsonProperty("event_date")
    private Date eventDate;

    @JsonProperty("ticket_info")
    private String ticketInfo;

    @JsonProperty("image")
    private String image;

    @JsonProperty("organiser")
    private String organiser;

    @JsonProperty("venue")
    private String venue;

    @JsonProperty("address")
    private String address;

    @JsonProperty("organiser_number")
    private String organiserNumber;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("description")
    private String description;

    @JsonProperty("city")
    private String city;

    @JsonProperty("remarks")
    private String remarks;

    @JsonProperty("event_time")
    private String eventTime;

}
