package com.spnra.partyApp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InternalUserResponse {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("user_type")
    private String userType;

    @JsonProperty("signup_status")
    private Integer signUpStatus;

    @JsonProperty("city_id")
    private Long cityId;

    @JsonProperty("image")
    private String image;

    @JsonProperty("email_verified")
    private Boolean emailVerified;

    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("share_token")
    private String shareToken;

    @JsonProperty("error_code")
    private Integer errorCode = 0;
}
