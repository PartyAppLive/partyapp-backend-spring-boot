package com.spnra.partyApp.model;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.Date;

@Slf4j
@Component
public class S3DocUploader {

    private AmazonS3 s3client;

    @Value(("${s3.bucket}"))
    private String s3Bucket;

    @Value(("${s3.key}"))
    private String s3Key;

    @Value(("${accessKey}"))
    private String accessKey;

    @Value(("${secretKey}"))
    private String secretKey;



    @PostConstruct
    private void initializeAmazon() {
        AWSCredentials awsCredentials =
                new BasicAWSCredentials(accessKey, secretKey);
        s3client =  AmazonS3ClientBuilder
                .standard()
                .withRegion(Regions.AP_SOUTH_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }


    public void uploadFileToS3(String localFileName,String s3Key) throws Exception {
        File file = new File(localFileName);
        try {
            PutObjectRequest request = (new PutObjectRequest(s3Bucket, s3Key , file).withCannedAcl(CannedAccessControlList.PublicRead));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/jpeg");
            request.setMetadata(metadata);
            s3client.putObject(request);
        } catch (AmazonServiceException ase) {
            log.info("AmazonServiceException Error while uploading File {} to S3 Bucket with Error Message: {}, HTTP Status Code: {}, AWS Error Code: {}, Error Type: {}, Request ID: {}",
                    localFileName, ase.getMessage(), ase.getStatusCode(), ase.getErrorCode(), ase.getErrorType(),
                    ase.getRequestId());
            throw ase;
        } catch (AmazonClientException ace) {
            log.error("AmazonClientException Error while uploading File {} to S3 Bucket with Error Message: {}",
                    localFileName, ace.getMessage());
            throw ace;
        }
        file.delete();
        log.info("Successful upload of {} File to S3 Bucket", localFileName);
    }

    public File convertMultiPartToFile(MultipartFile file) throws IOException {
        File conFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(conFile);
        fos.write(file.getBytes());
        fos.close();
        return conFile;
    }

}
