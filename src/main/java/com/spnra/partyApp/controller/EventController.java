package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.ClubInterestRequest;
import com.spnra.partyApp.dto.request.EventInterestRequest;
import com.spnra.partyApp.dto.request.EventRequest;
import com.spnra.partyApp.dto.request.ReservationRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.service.EventService;
import com.spnra.partyApp.service.ReservationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @PostMapping(value = URIEndpoints.CREATE_EVENT)
    public ResponseEntity<?> createEvent(@Valid @RequestBody EventRequest eventRequest) throws Exception {
        log.info("Received Event Create Request: {}", eventRequest.toString());
        return new ResponseEntity<>(eventService.createEvent(eventRequest), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.EVENT_INTEREST)
    public ResponseEntity<?> eventInterest(@RequestBody EventInterestRequest eventInterestRequest) throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Event Interest Create Request: {}", eventInterestRequest.toString());
        return new ResponseEntity<>(eventService.eventInterest(eventInterestRequest,authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.REMOVE_EVENT_INTEREST)
    public ResponseEntity<?> removeInterest(@RequestBody EventInterestRequest eventInterestRequest) throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Event Interest Remove Request: {}", eventInterestRequest.toString());
        return new ResponseEntity<>(eventService.removeEventInterest(eventInterestRequest,authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.EVENTS)
    public ResponseEntity<?> getAllEvents(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "1") Long cityId)
            throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        return new ResponseEntity<>(eventService.getAllEvents(authorisation,page,cityId), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.EVENTS_TODAY)
    public ResponseEntity<?> getTodayEvents(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "1") Long cityId)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        return new ResponseEntity<>(eventService.getTodayEvents(authorisation,page,cityId), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.EVENT_PEOPLE_INTEREST)
    public ResponseEntity<?> peopleInterest(@RequestParam(value = "event_id") Integer eventId,@RequestParam(value = "page",defaultValue = "0") int page) throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Event People Interest Create Request: {}", eventId.toString());
        return new ResponseEntity<>(eventService.peopleInterest(eventId,authorisation,page), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.SEARCH_EVENTS)
    public ResponseEntity<?> searchEvents(@RequestParam(value = "key_word") String keyWord) throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received favourite page Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(eventService.searchEvent(keyWord), HttpStatus.OK);
    }
}
