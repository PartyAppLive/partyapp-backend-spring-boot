package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.CancelReservationRequest;
import com.spnra.partyApp.dto.request.ReservationRequest;
import com.spnra.partyApp.dto.request.UserRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.repo.ReservationRepo;
import com.spnra.partyApp.service.ReservationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @UserAuthentication
    @PostMapping(value = URIEndpoints.RESERVATION)
    public ResponseEntity<?> createReservation(@RequestBody ReservationRequest reservationRequest) throws Exception {
        log.info("Received Reservation Create Request: {}", reservationRequest.toString());
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        return new ResponseEntity<>(reservationService.reserve(reservationRequest,authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_RESERVATIONS)
    public ResponseEntity<?> getAllReservations(@RequestParam(defaultValue = "0") int page)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received get all reservation Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(reservationService.getReservations(authorisation,page), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.CANCEL_RESERVATION)
    public ResponseEntity<?> cancelReservation(@RequestBody CancelReservationRequest cancelReservationRequest) throws Exception {
        log.info("Received Reservation cancel Request: {}", cancelReservationRequest.toString());
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        return new ResponseEntity<>(reservationService.cancelReservation(cancelReservationRequest,authorisation), HttpStatus.OK);
    }
}
