package com.spnra.partyApp.controller.Admin;

import com.spnra.partyApp.auth.*;
import com.spnra.partyApp.constants.AdminURIEndPoints;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.Admin.AdminUserRequest;
import com.spnra.partyApp.dto.request.OtpVerificationRequest;
import com.spnra.partyApp.dto.request.SignInRequest;
import com.spnra.partyApp.dto.request.UserProfileUpdateRequest;
import com.spnra.partyApp.dto.request.UserRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.exceptions.InValidRequestException;
import com.spnra.partyApp.service.AdminUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = AdminURIEndPoints.PARTY_APP_ADMIN_BASE_URL)
public class AdminUserController {

    @Autowired
    private ClubAdminAuthInterceptor clubAdminAuthInterceptor;

    @Autowired
    private AdminUserService adminUserService;

    @PostMapping(value = AdminURIEndPoints.CLUB_CREATE_USER)
    public ResponseEntity<?> createUser(@RequestBody AdminUserRequest adminUserRequest) throws Exception {
        log.info("Received User Create Request: {}", adminUserRequest.toString());
        return new ResponseEntity<>(adminUserService.createClubAdmin(adminUserRequest), HttpStatus.OK);
    }

    @PostMapping(value = AdminURIEndPoints.CLUB_SIGN_IN)
    public ResponseEntity<?> signIn(@RequestBody SignInRequest signInRequest) throws Exception {
        log.info("Received Sign In  Request: {}", signInRequest.toString());
        if(ObjectUtils.isEmpty(signInRequest.getPhone())){
            throw new InValidRequestException("Missing details");
        }
        return new ResponseEntity<>(adminUserService.signInClubAdmin(signInRequest.getPhone()), HttpStatus.OK);

    }

    @ClubAdminAuthentication
    @GetMapping(value = AdminURIEndPoints.CLUB_SIGN_OUT)
    public ResponseEntity<?> signOut() throws Exception {
        Authorisation authorisation = clubAdminAuthInterceptor.getAuthorisationDetails();
        log.info("Received Sign Out Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(adminUserService.signOutClubAdmin(authorisation.getId()), HttpStatus.OK);

    }

    @PostMapping(value = AdminURIEndPoints.CLUB_VERIFY_OTP)
    public ResponseEntity<?> verifyOtp(@RequestBody OtpVerificationRequest otpVerificationRequest) throws Exception {
        log.info("Received Otp Verification Request: {}", otpVerificationRequest.toString());
        return new ResponseEntity<>(adminUserService.otpVerifyClubAdmin(otpVerificationRequest), HttpStatus.OK);
    }

    @PostMapping(value = AdminURIEndPoints.CLUB_RESEND_OTP)
    public ResponseEntity<?> resendOtp(@RequestBody SignInRequest signInRequest) throws Exception {
        if(ObjectUtils.isEmpty(signInRequest.getPhone())){
            throw new InValidRequestException("Missing details");
        }
        log.info("Received Otp Resend Request: {}", signInRequest.toString());
        return new ResponseEntity<>(adminUserService.otpResendClubAdmin(signInRequest.getPhone()), HttpStatus.OK);
    }

    @ClubAdminAuthentication
    @PostMapping(value = AdminURIEndPoints.CLUB_PROFILE_UPDATE)
    public ResponseEntity<?> profileUpdate(@RequestBody UserProfileUpdateRequest request) throws Exception {
        Authorisation authorisation = clubAdminAuthInterceptor.getAuthorisationDetails();
        log.info("Received Profile update Request for user = {}",authorisation.getUserId());
        return new ResponseEntity<>(adminUserService.updateClubDetails(request,authorisation), HttpStatus.OK);
    }

    @ClubAdminAuthentication
    @GetMapping(value = AdminURIEndPoints.CLUB_GET_USER)
    public ResponseEntity<?> getUser() throws Exception {
        Authorisation authorisation = clubAdminAuthInterceptor.getAuthorisationDetails();
        log.info("Received Get User Request for user = {}",authorisation.getUserId());
        return new ResponseEntity<>(adminUserService.getClubAdmin(authorisation.getUserId(),null), HttpStatus.OK);
    }
}
