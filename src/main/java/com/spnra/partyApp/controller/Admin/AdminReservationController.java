package com.spnra.partyApp.controller.Admin;

import com.spnra.partyApp.auth.*;
import com.spnra.partyApp.constants.AdminURIEndPoints;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.CancelReservationRequest;
import com.spnra.partyApp.dto.request.UpdateReservationRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.service.ReservationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = AdminURIEndPoints.PARTY_APP_ADMIN_BASE_URL)
public class AdminReservationController {

    @Autowired
    private ClubAdminAuthInterceptor clubAdminAuthInterceptor;

    @Autowired
    private ReservationService reservationService;

//    @ClubAdminAuthentication
//    @PostMapping(value = AdminURIEndPoints.UPDATE_RESERVATION)
//    public ResponseEntity<?> updateReservation(@RequestBody UpdateReservationRequest updateReservationRequest) throws Exception {
//        log.info("Received Reservation cancel Request: {}", updateReservationRequest.toString());
//        Authorisation authorisation = clubAdminAuthInterceptor.getAuthorisationDetails();
//        return new ResponseEntity<>(reservationService.updateReservation(updateReservationRequest,authorisation), HttpStatus.OK);
//    }

}
