package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.OfferAddRequest;
import com.spnra.partyApp.dto.request.TicketCreationRequest;
import com.spnra.partyApp.dto.request.TicketOrderRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class CartController {

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @Autowired
    private CartService cartService;

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_CART)
    public ResponseEntity<?> getCart() throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Get cart Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(cartService.getCart(authorisation.getUserId()), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.ADD_PRODUCT)
    public ResponseEntity<?> addProduct(@RequestBody OfferAddRequest offerAddRequest) throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Add Product Request: {}", offerAddRequest.toString());
        return new ResponseEntity<>(cartService.addProduct(offerAddRequest,authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.REMOVE_PRODUCT)
    public ResponseEntity<?> removeProduct(@RequestBody OfferAddRequest offerAddRequest) throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Club Offer Create Request: {}", offerAddRequest.toString());
        return new ResponseEntity<>(cartService.removeProduct(offerAddRequest,authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.PLACE_ORDER)
    public ResponseEntity<?> placeOrder(){
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Place Order Request for userID: {}", authorisation.getUserId());
        return new ResponseEntity<>(cartService.placeOrder(authorisation.getUserId()), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.PLACE_TICKET_ORDER)
    public ResponseEntity<?> placeTicketOrder(@RequestBody TicketOrderRequest ticketOrderRequest){
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Place Order Request for userID: {}", authorisation.getUserId());
        return new ResponseEntity<>(cartService.placeTicketOrder(authorisation.getUserId(),ticketOrderRequest), HttpStatus.OK);
    }
}
