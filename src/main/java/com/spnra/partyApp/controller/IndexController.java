package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.service.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class IndexController {

    @Autowired
    private IndexService indexService;

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_CLUBS)
    public ResponseEntity<?> getAllClubs(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "1") Long cityId)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received get all clubs Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(indexService.getAllClubs(authorisation,page,cityId), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_OFFER_CLUBS)
    public ResponseEntity<?> getAllOfferClubs(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "1") Long cityId)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received get all offer clubs Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(indexService.getAllOfferClubs(authorisation,page,cityId), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_LOCATIONS)
    public ResponseEntity<?> getAllLocations()throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received get all locations Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(indexService.getLocations(), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.CONTACT_US)
    public ResponseEntity<?> getContactUs()throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received get contact us Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(indexService.getContactUs(), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_CLUB)
    public ResponseEntity<?> getPdpPageDetails(@PathVariable("club_id") Long clubId)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Club Pdp page Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(indexService.getClub(authorisation,clubId), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.FAVOURITE_CLUBS)
    public ResponseEntity<?> getAllFavouriteClubs(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "1") Long cityId) throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received favourite page Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(indexService.getAllFavouriteClubs(authorisation,page,cityId), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.SEARCH_CLUBS)
    public ResponseEntity<?> searchClubs(@RequestParam(value = "key_word") String keyWord,@RequestParam(value = "city_id") Long cityId) throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received favourite page Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(indexService.searchClub(cityId,keyWord), HttpStatus.OK);
    }
}
