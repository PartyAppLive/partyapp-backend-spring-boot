package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class PaymentController {

}
