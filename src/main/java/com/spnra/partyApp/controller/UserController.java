package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.*;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.exceptions.InValidRequestException;
import com.spnra.partyApp.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @PostMapping(value = URIEndpoints.CREATE_USER)
    public ResponseEntity<?> createUser(@RequestBody UserRequest userRequest) throws Exception {
        log.info("Received User Create Request: {}", userRequest.toString());
        return new ResponseEntity<>(userService.createUser(userRequest), HttpStatus.OK);
    }

    @PostMapping(value = URIEndpoints.SIGN_IN)
    public ResponseEntity<?> signIn(@RequestBody SignInRequest signInRequest) throws Exception {
        log.info("Received Sign In  Request: {}", signInRequest.toString());
        if(ObjectUtils.isEmpty(signInRequest.getPhone())){
            throw new InValidRequestException("Missing details");
        }
        return new ResponseEntity<>(userService.signIn(signInRequest.getPhone()), HttpStatus.OK);

    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.SIGN_OUT)
    public ResponseEntity<?> signOut() throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Sign Out Request: {}", authorisation.getUserId());
        return new ResponseEntity<>(userService.signOut(authorisation.getId()), HttpStatus.OK);

    }

    @PostMapping(value = URIEndpoints.VERIFY_OTP)
    public ResponseEntity<?> verifyOtp(@RequestBody OtpVerificationRequest otpVerificationRequest) throws Exception {
        log.info("Received Otp Verification Request: {}", otpVerificationRequest.toString());
        return new ResponseEntity<>(userService.otpVerify(otpVerificationRequest), HttpStatus.OK);
    }

    @PostMapping(value = URIEndpoints.RESEND_OTP)
    public ResponseEntity<?> resendOtp(@RequestBody SignInRequest signInRequest) throws Exception {
        if(ObjectUtils.isEmpty(signInRequest.getPhone())){
            throw new InValidRequestException("Missing details");
        }
        log.info("Received Otp Resend Request: {}", signInRequest.toString());
        return new ResponseEntity<>(userService.otpResend(signInRequest.getPhone()), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.IMAGE_UPLOAD)
    public ResponseEntity<?> imageUpload(MultipartHttpServletRequest request) throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Image upload Request for user = {}",authorisation.getUserId());
        return new ResponseEntity<>(userService.dpUpload(authorisation.getUserId(),request.getFileMap()), HttpStatus.OK);
    }

    @UserAuthentication
    @PatchMapping(value = URIEndpoints.PROFILE_UPDATE)
    public ResponseEntity<?> profileUpdate(@RequestBody UserProfileUpdateRequest request) throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Profile update Request for user = {}",authorisation.getUserId());
        return new ResponseEntity<>(userService.profileUpdate(request,authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_USER)
    public ResponseEntity<?> getUser() throws Exception {
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Get User Request for user = {}",authorisation.getUserId());
        return new ResponseEntity<>(userService.getUser(authorisation.getUserId(),null), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.SELECT_LOCATION)
    public ResponseEntity<?> selectLocation(@RequestBody CitySelectRequest request) throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received favourite page Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(userService.selectCity(request,authorisation), HttpStatus.OK);
    }




}
