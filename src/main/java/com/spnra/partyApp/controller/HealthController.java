package com.spnra.partyApp.controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class HealthController {

    @GetMapping(value = "/v1/health")
    public String dummy() {
        return "Party App API welcomes you !!";
    }
}
