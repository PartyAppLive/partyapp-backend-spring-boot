package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.ClubAdminAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.ClubCreationRequest;
import com.spnra.partyApp.dto.request.OfferCreationRequest;
import com.spnra.partyApp.dto.request.TicketCreationRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.service.OfferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class OfferController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @PostMapping(value = URIEndpoints.CREATE_OFFER)
    public ResponseEntity<?> createOffer(@RequestBody OfferCreationRequest offerCreationRequest) throws Exception {
        log.info("Received Club Offer Create Request: {}", offerCreationRequest.toString());
        return new ResponseEntity<>(offerService.createOffer(offerCreationRequest), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_OFFERS)
    public ResponseEntity<?> getAllOffers(@PathVariable("club_id") Long clubId)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Get all offers for user: {}",authorisation.getUserId());
        return new ResponseEntity<>(offerService.getOffer(authorisation,clubId), HttpStatus.OK);
    }

    @PostMapping(value = URIEndpoints.CREATE_TICKET)
    public ResponseEntity<?> createTicket(@RequestBody TicketCreationRequest ticketCreationRequest) throws Exception {
        log.info("Received Club Offer Create Request: {}", ticketCreationRequest.toString());
        return new ResponseEntity<>(offerService.createTicket(ticketCreationRequest), HttpStatus.OK);
    }

    @UserAuthentication
    @GetMapping(value = URIEndpoints.GET_TICKET)
    public ResponseEntity<?> getTicket(@PathVariable("event_id") Long eventId)throws Exception{
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        log.info("Received Get all offers page Request: {}",authorisation.getUserId());
        return new ResponseEntity<>(offerService.getTicket(eventId), HttpStatus.OK);
    }




}
