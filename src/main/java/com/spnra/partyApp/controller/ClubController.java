package com.spnra.partyApp.controller;

import com.spnra.partyApp.auth.ClientAuthentication;
import com.spnra.partyApp.auth.UserAuthInterceptor;
import com.spnra.partyApp.auth.UserAuthentication;
import com.spnra.partyApp.constants.URIEndpoints;
import com.spnra.partyApp.dto.request.ClubCreationRequest;
import com.spnra.partyApp.dto.request.ClubDetailCreationRequest;
import com.spnra.partyApp.dto.request.ClubInfoUpdateRequest;
import com.spnra.partyApp.dto.request.ClubInterestRequest;
import com.spnra.partyApp.entity.Authorisation;
import com.spnra.partyApp.service.ClubService;
import com.spnra.partyApp.service.IndexService;
import com.spnra.partyApp.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Slf4j
@RestController
@ClientAuthentication
@RequestMapping(value = URIEndpoints.PARTY_APP_BASE_URL)
public class ClubController {

    @Autowired
    private ClubService clubService;

    @Autowired
    private UserAuthInterceptor userAuthInterceptor;

    @PostMapping(value = URIEndpoints.CREATE_CLUB)
    public ResponseEntity<?> createClub(@RequestBody ClubCreationRequest clubCreationRequest) throws Exception {
        log.info("Received Event Club Create Request: {}", clubCreationRequest.toString());
        return new ResponseEntity<>(clubService.createClub(clubCreationRequest), HttpStatus.OK);
    }

    @PostMapping(value = URIEndpoints.CREATE_CLUB_DETAIL)
    public ResponseEntity<?> createClubDetail(@RequestBody ClubDetailCreationRequest clubDetailCreationRequest) throws Exception {
        log.info("Received Club Detail Create Request: {}", clubDetailCreationRequest.toString());
        return new ResponseEntity<>(clubService.createClubDetail(clubDetailCreationRequest), HttpStatus.OK);
    }

    @PostMapping(value = URIEndpoints.CLUB_IMAGE_INFO_UPDATE)
    public ResponseEntity<?> clubInfoUpdate(@RequestBody ClubInfoUpdateRequest clubInfoUpdateRequest) throws Exception {
        log.info("Received club Info upload Request = {}",clubInfoUpdateRequest.toString());
        return new ResponseEntity<>(clubService.updateClubImages(clubInfoUpdateRequest), HttpStatus.OK);
    }

    @PostMapping(value = URIEndpoints.CLUB_IMAGES_UPLOAD)
    public ResponseEntity<?> imageUploadExtra(MultipartHttpServletRequest request) throws Exception {
        log.info("Received Club image upload Request");
        return new ResponseEntity<>(clubService.uploadClubImages(request.getFileMap()), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.CLUB_INTEREST)
    public ResponseEntity<?> clubInterest(@RequestBody ClubInterestRequest clubInterestRequest) throws Exception {
        log.info("Received Club Interest create Request: {}", clubInterestRequest.toString());
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        return new ResponseEntity<>(clubService.clubInterest(clubInterestRequest.getClubId(),authorisation), HttpStatus.OK);
    }

    @UserAuthentication
    @PostMapping(value = URIEndpoints.REMOVE_CLUB_INTEREST)
    public ResponseEntity<?> removeInterest(@RequestBody ClubInterestRequest clubInterestRequest) throws Exception {
        log.info("Received Club Interest Remove Request: {}", clubInterestRequest.toString());
        Authorisation authorisation = userAuthInterceptor.getAuthorisationDetails();
        return new ResponseEntity<>(clubService.removeClubInterest(clubInterestRequest.getClubId(),authorisation), HttpStatus.OK);
    }


}
