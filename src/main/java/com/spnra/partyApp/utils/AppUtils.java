package com.spnra.partyApp.utils;

import com.spnra.partyApp.entity.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class AppUtils {

    public static Boolean phoneNumberValidate(String phone){
        String regex = "^(\\d{3}[- ]?){2}\\d{4}$";;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phone);
        Boolean check = matcher.matches();
        return check;
    }

}