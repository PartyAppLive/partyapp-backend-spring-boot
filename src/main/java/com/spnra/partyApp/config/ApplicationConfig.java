package com.spnra.partyApp.config;


import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

//@Import({UserTrackingConfig.class})
@Configuration
public class ApplicationConfig {

    @Value("${rest.template.connect.timeout}")
    private Integer CONNECT_TIMEOUT;

    @Value("${rest.template.connect.request.timeout}")
    private Integer CONNECT_REQUEST_TIMEOUT;

    @Value("${rest.template.read.timeout}")
    private Integer READ_TIMEOUT;

    @Value("${rest.template.max.conn.per.route}")
    private Integer MAX_CONN_PER_ROUTE;

    @Value("${rest.template.max.conn}")
    private Integer MAX_CONN;


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

        connectionManager.setDefaultMaxPerRoute(MAX_CONN_PER_ROUTE);
        connectionManager.setMaxTotal(MAX_CONN);
        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory(
                        HttpClientBuilder.create().setConnectionManager(connectionManager).build());
        requestFactory.setConnectionRequestTimeout(CONNECT_REQUEST_TIMEOUT);
        requestFactory.setConnectTimeout(CONNECT_TIMEOUT);
        requestFactory.setReadTimeout(READ_TIMEOUT);

        RestTemplate template = builder.detectRequestFactory(false).build();
        template.setRequestFactory(requestFactory);
        return template;
    }
}
