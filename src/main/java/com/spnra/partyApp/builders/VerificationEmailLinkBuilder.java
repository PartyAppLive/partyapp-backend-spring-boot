package com.spnra.partyApp.builders;

public class VerificationEmailLinkBuilder {
    public static String getEmailVerificationUrl(String baseUrl, String token){

        return baseUrl + "?verification_token=" + token;

    }
}