package com.spnra.partyApp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ReservationStatus {
    CONFIRMATION_PENDING("Pending"),
    CANCELLED_BY_USER("cancel"),
    REJECTED_BY_CLUB("Rejected"),
    CONFIRMED("Confirm"),
    NO_SHOW("no_show");

    private String value;

    public String getType() {
        return value;
    }
}
