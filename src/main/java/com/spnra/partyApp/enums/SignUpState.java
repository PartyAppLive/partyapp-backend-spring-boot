package com.spnra.partyApp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum SignUpState {
    DOWNLOAD(1),
    OTP_SENT(2),
    OTP_VERIFIED(3);
    private Integer value;
}
