package com.spnra.partyApp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum UserStatus {

    ACTIVE(true),
    INACTIVE(false);

    private Boolean value;
}
