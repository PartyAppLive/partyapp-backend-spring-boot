package com.spnra.partyApp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum City {
    SILIGURI(1L),
    BANGALORE(2L),
    GOA(3L);


    private Long value;

    public Long getType() {
        return value;
    }

}
