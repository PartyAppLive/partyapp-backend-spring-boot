package com.spnra.partyApp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum UserType {
    NORMAL(1),
    ADMIN(2);

    private Integer value;

}
