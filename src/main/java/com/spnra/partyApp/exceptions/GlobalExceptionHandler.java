package com.spnra.partyApp.exceptions;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {BaseException.class})
    public ResponseEntity<JsonNode> handleError(BaseException e) {
        ObjectNode result = JsonNodeFactory.instance.objectNode();
        result.put("error_code", e.getErrorCode().getValue());
        result.put("message", e.getMessage());
        return new ResponseEntity<>(result, e.getHttpStatusCode());
    }
}
