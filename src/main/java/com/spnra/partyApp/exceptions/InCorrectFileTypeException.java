package com.spnra.partyApp.exceptions;

import com.spnra.partyApp.enums.ErrorCode;
import org.springframework.http.HttpStatus;

public class InCorrectFileTypeException extends BaseException {
    public InCorrectFileTypeException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.IN_CORRECT_FILE_TYPE;
    }
}

