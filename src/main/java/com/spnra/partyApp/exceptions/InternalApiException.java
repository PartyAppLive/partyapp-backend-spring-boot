package com.spnra.partyApp.exceptions;

import com.spnra.partyApp.enums.ErrorCode;
import org.springframework.http.HttpStatus;

public class InternalApiException extends BaseException {

    public InternalApiException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.INTERNAL_API_ERROR;
    }
}
