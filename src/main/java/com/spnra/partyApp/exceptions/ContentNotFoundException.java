package com.spnra.partyApp.exceptions;

import com.spnra.partyApp.enums.ErrorCode;
import org.springframework.http.HttpStatus;

public class ContentNotFoundException extends BaseException{
    public ContentNotFoundException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.CONTENT_NOT_FOUND;
    }
}
