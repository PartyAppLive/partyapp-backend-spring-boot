package com.spnra.partyApp.exceptions;

import org.springframework.http.HttpStatus;

public class ErrorResponseException extends Exception {

    private HttpStatus httpStatus;

    private String response;

    public ErrorResponseException(String response, HttpStatus httpStatus) {
        super("Error response from remote");
        this.httpStatus = httpStatus;
        this.response = response;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getResponse() {
        return response;
    }
}
