package com.spnra.partyApp.exceptions;

import com.spnra.partyApp.enums.ErrorCode;
import org.springframework.http.HttpStatus;

public abstract class BaseException extends Exception {

    public BaseException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return getClass().getName() + "(message=" + getMessage() + ")";
    }

    public abstract HttpStatus getHttpStatusCode();

    public abstract ErrorCode getErrorCode();
}
