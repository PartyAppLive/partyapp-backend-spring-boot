package com.spnra.partyApp.exceptions;

import com.spnra.partyApp.enums.ErrorCode;
import org.springframework.http.HttpStatus;

public class DifferentClubOfferException extends BaseException {

    public DifferentClubOfferException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getHttpStatusCode() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.DIFFERENT_CLUB_OFFER;
    }
}
