CREATE TABLE IF NOT EXISTS token(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id Int(11) NOT NULL,
    user_type Int(4) NOT NULL,
    attempt Int(11) NOT NULL,
    otp int(10) NOT NULL,
    type varchar(255),
    created_at DATETIME,
    status BOOLEAN NOT NULL default 1,
    updated_at DATETIME,
    expiry_time DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=1;