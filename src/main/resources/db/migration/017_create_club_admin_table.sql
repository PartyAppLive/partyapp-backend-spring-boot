CREATE TABLE IF NOT EXISTS admin_user(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    phone_number varchar(20) UNIQUE NOT NULL,
    email varchar(255),
    club_id INT(11) NOT NULL,
    sign_up_status INT(11) default 1,
    status BOOLEAN NOT NULL default 1 ,
    email_verified BOOLEAN NOT NULL default 0,
    created_at DATETIME,
    updated_at DATETIME
    )ENGINE=InnoDB AUTO_INCREMENT=1;