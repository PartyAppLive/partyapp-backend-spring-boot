CREATE TABLE IF NOT EXISTS cart_details(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cart_id INT(11) NOT NULL,
    offer_id INT(11) NOT NULL,
    offer_type varchar(20) NOT NULL,
    user_id INT(11) NOT NULL,
    quantity INT(8) NOT NULL,
    created_at DATETIME,
    updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=1;