CREATE TABLE IF NOT EXISTS ticket(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(200) NOT NULL,
    event_id INT(11) NOT NULL,
    image varchar(200) NOT NULL,
    price INT(8) NOT NULL,
    inventory INT(8) NOT NULL,
    max_quantity INT(8) NOT NULL,
    sold_unit INT(8) NOT NULL,
    status BOOLEAN NOT NULL default 0,
    start_date DATE,
    end_date DATE,
    info varchar(250) NOT NULL,
    terms varchar(250) NOT NULL,
    created_at DATETIME,
    updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=1;