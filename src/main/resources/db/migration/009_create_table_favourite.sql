CREATE TABLE IF NOT EXISTS favourite(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id Int(11) NOT NULL,
    club_id INT(11) NOT NULL,
    city_id INT(11) NOT NULL,
    created_at DATETIME,
    updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=1;