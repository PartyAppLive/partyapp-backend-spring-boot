CREATE TABLE IF NOT EXISTS reservation(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id INT(11) NOT NULL,
    club_id INT(11) NOT NULL,
    club_name varchar(255) NOT NULL,
    persons INT(11) NOT NULL,
    booked_date DATE,
    booked_time TIME,
    created_at DATETIME,
    updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=1;