CREATE TABLE IF NOT EXISTS detail(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    club_id INT(11) UNIQUE NOT NULL,
    description varchar(255),
    kitchen_text varchar(255),
    beer_text varchar(255),
    kitchen_rate INT(11),
    beer_rate INT(11),
    valet BOOLEAN NOT NULL default 0,
    menu longtext,
    stags longtext,
    timings longtext,
    day_special longtext,
    photos longtext,
    created_at DATETIME,
    updated_at DATETIME
)ENGINE=InnoDB AUTO_INCREMENT=1;